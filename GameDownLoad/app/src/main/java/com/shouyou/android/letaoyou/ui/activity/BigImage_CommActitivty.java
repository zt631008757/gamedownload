package com.shouyou.android.letaoyou.ui.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.shouyou.android.letaoyou.R;
import com.shouyou.android.letaoyou.adapter.BigImageCommPageAdapter;

import java.util.List;

/**
 * Created by Administrator on 2018/9/4.
 * 大图页
 */

public class BigImage_CommActitivty extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        imgs = getIntent().getStringArrayListExtra("imgs");
        currentIndex = getIntent().getIntExtra("currentIndex", 0);
        setContentView(R.layout.activity_bigimage);
        initView();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Glide.get(this).clearMemory();
        System.gc();
    }

    List<String> imgs;
    int currentIndex = 0;

    TextView tv_page;
    ViewPager viewpager;
    BigImageCommPageAdapter adapter;

    private void initView() {
        tv_page = findViewById(R.id.tv_page);
        viewpager = findViewById(R.id.viewpager);

        adapter = new BigImageCommPageAdapter(mContext, imgs);
        viewpager.setOffscreenPageLimit(imgs.size() - 2);
        viewpager.setAdapter(adapter);

        viewpager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                tv_page.setText((viewpager.getCurrentItem() + 1) + "/" + imgs.size());
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        viewpager.setCurrentItem(currentIndex);

        tv_page.setText((currentIndex + 1) + "/" + imgs.size());

        adapter.setPhotoViewClickListener(new BigImageCommPageAdapter.PhotoViewClickListener() {
            @Override
            public void OnPhotoTapListener(View view, float v, float v1) {
                finish();
            }
        });

        if (imgs != null && imgs.size() == 1) {
            tv_page.setVisibility(View.GONE);
        }
    }
}
