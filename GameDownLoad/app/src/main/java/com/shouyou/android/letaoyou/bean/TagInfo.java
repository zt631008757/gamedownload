package com.shouyou.android.letaoyou.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Administrator on 2018/12/16 0016.
 */

public class TagInfo implements Serializable{
    public String id;//": 1,
    public String parent_id;//": 0,
    public String name;//": "角色扮演",
    public String logo;//": "",
    public String created_at;//": "2018-08-17 00:00:00",
    public String updated_at;//": null,

    public List<GameInfo> limit_applications;
}
