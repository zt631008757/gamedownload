package com.shouyou.android.letaoyou.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.shouyou.android.letaoyou.R;
import com.shouyou.android.letaoyou.dialog.CommDialog;
import com.shouyou.android.letaoyou.manager.UserManager;
import com.shouyou.android.letaoyou.tool.CommToast;
import com.shouyou.android.letaoyou.ui.view.MultiStateView;

/**
 * Created by Administrator on 2018/6/20.
 */

public class SettingActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        initView();
        getData();
    }

    LinearLayout ll_about, ll_downloadcenter;
    TextView tv_logout;

    private void initView() {
        setTitle("设置");
        setLeftImgClickListener();

        ll_about = findViewById(R.id.ll_about);
        tv_logout = findViewById(R.id.tv_logout);
        ll_downloadcenter = findViewById(R.id.ll_downloadcenter);

        ll_about.setOnClickListener(this);
        tv_logout.setOnClickListener(this);
        ll_downloadcenter.setOnClickListener(this);

        if (UserManager.isLogin(mContext)) {
            tv_logout.setVisibility(View.VISIBLE);
        } else {
            tv_logout.setVisibility(View.GONE);
        }
    }

    private void getData() {


    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        Intent intent;
        switch (view.getId()) {
            case R.id.ll_about:
                intent = new Intent(mContext, WebViewActivity.class);
                intent.putExtra("title", "关于我们");
                intent.putExtra("url", "http://h5.eqxiu.com/ls/FQIJgT0A");
                startActivity(intent);
                break;
            case R.id.ll_downloadcenter:
                intent = new Intent(mContext, DownLoadListActivity.class);
                startActivity(intent);
                break;
            case R.id.tv_logout:
                CommDialog.showCommDialog(mContext, "", "退出登录", "取消", "确认退出登录", null, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        UserManager.logout(mContext);
                        if (UserManager.isLogin(mContext)) {
                            tv_logout.setVisibility(View.VISIBLE);
                        } else {
                            tv_logout.setVisibility(View.GONE);
                        }
                        CommToast.showToast(mContext, "已退出登录");
                    }
                });
                break;
        }
    }
}
