package com.shouyou.android.letaoyou.bean;

import java.io.Serializable;

/**
 * Created by Administrator on 2018/12/23.
 */

public class WenZhangInfo implements Serializable {

    public String id;//": 2,
    public String admin_id;//": 0,
    public String link_id;//": 1,
    public String picture;//": "images/36fcbdb4e9ce3c030e46092ab5e65647.jpg",
    public String title;//": "222222",
    public String subtitle;
    public String body;
    public String updated_at;
    public LinkeInfo game;

    public class LinkeInfo implements Serializable{
        public String id;//": 1,
        public String admin_id;//": 2,
        public String application_id;//": 3,
        public String link;//": "https://imgcdn.juefeng.com//sdks/870/870_dfzj_80142.apk",
        public String created_at;//": "2018-09-24 18:48:39",
        public String updated_at;//": "2018-09-24 18:48:39",
        public GameInfo application;
    }
}
