package com.shouyou.android.letaoyou.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.shouyou.android.letaoyou.R;
import com.shouyou.android.letaoyou.bean.UserInfo;
import com.shouyou.android.letaoyou.interface_.OkHttpCallBack;
import com.shouyou.android.letaoyou.manager.API_LoginManager;
import com.shouyou.android.letaoyou.manager.UserManager;
import com.shouyou.android.letaoyou.responce.BaseResponce;
import com.shouyou.android.letaoyou.responce.RegistResponce;
import com.shouyou.android.letaoyou.tool.CommToast;
import com.shouyou.android.letaoyou.tool.Log;

import java.util.HashMap;

import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.PlatformActionListener;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.tencent.qq.QQ;
import cn.sharesdk.wechat.friends.Wechat;

/**
 * Created by Administrator on 2018/6/20.
 */

public class LoginActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initView();
        getData();
    }

    EditText et_account, et_pwd;
    TextView tv_login, tv_toregist;
    ImageView btn_login_weixin, btn_login_qq, iv_delete_account, iv_delete_pwd;

    private void initView() {
        setTitle("登录");
        setLeftImgClickListener();

        et_account = findViewById(R.id.et_account);
        et_pwd = findViewById(R.id.et_pwd);
        tv_login = findViewById(R.id.tv_login);
        tv_toregist = findViewById(R.id.tv_toregist);
        btn_login_qq = (ImageView) findViewById(R.id.btn_login_qq);
        btn_login_weixin = (ImageView) findViewById(R.id.btn_login_weixin);

        iv_delete_account = findViewById(R.id.iv_delete_account);
        iv_delete_pwd = findViewById(R.id.iv_delete_pwd);

        tv_toregist.setOnClickListener(this);
        tv_login.setOnClickListener(this);
        btn_login_qq.setOnClickListener(this);
        btn_login_weixin.setOnClickListener(this);

        iv_delete_account.setOnClickListener(this);
        iv_delete_pwd.setOnClickListener(this);

        et_account.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    iv_delete_account.setVisibility(View.VISIBLE);
                } else {
                    iv_delete_account.setVisibility(View.GONE);
                }
            }
        });
        et_pwd.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    iv_delete_pwd.setVisibility(View.VISIBLE);
                } else {
                    iv_delete_pwd.setVisibility(View.GONE);
                }
            }
        });
    }

    private void getData() {

    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.tv_login:
                login();
                break;
            case R.id.tv_toregist:
                Intent intent = new Intent(mContext, RegistActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.btn_login_qq:
                doThridLogin(QQ.NAME);
                break;
            case R.id.btn_login_weixin:
                doThridLogin(Wechat.NAME);
                break;
            case R.id.iv_delete_account:
                et_account.setText("");
                break;
            case R.id.iv_delete_pwd:
                et_pwd.setText("");
                break;
        }
    }


    private void login() {
        String account = et_account.getText().toString().trim();
        String pwd = et_pwd.getText().toString().trim();
        if (TextUtils.isEmpty(account)) {
            CommToast.showToast(mContext, "请输入账号");
            return;
        }
        if (TextUtils.isEmpty(pwd)) {
            CommToast.showToast(mContext, "请输入密码");
            return;
        }

        showLoadingDialog("");
        API_LoginManager.login(mContext, account, pwd, new OkHttpCallBack() {
            @Override
            public void onSuccess(String baseResponce) {
                dismissLoadingDialog();
                try {
                    RegistResponce registResponce = new Gson().fromJson(baseResponce, RegistResponce.class);
                    UserManager.saveToken(mContext, registResponce.access_token);
                    CommToast.showToast(mContext, "登录成功");
                    finish();
                } catch (Exception e) {
                    CommToast.showToast(mContext, "服务器错误");
                }
            }

            @Override
            public void onFailure(String baseResponce) {
                dismissLoadingDialog();
                CommToast.showToast(mContext, "请求失败，请重试");
            }
        });
    }

    //第三方登录
    //第三方登录 微信  微博 QQ
    private void doThridLogin(final String name) {
        final Platform plat = ShareSDK.getPlatform(name);
        plat.removeAccount(true); //移除授权状态和本地缓存，下次授权会重新授权
        plat.SSOSetting(false); //SSO授权，传false默认是客户端授权，没有客户端授权或者不支持客户端授权会跳web授权
        //授权回调监听，监听oncomplete，onerror，oncancel三种状态
        plat.setPlatformActionListener(new PlatformActionListener() {
            @Override
            public void onComplete(Platform platform, int action, HashMap<String, Object> hashMap) {
                if (action == Platform.ACTION_USER_INFOR) {
//                    CommToast.showToast(mContext, "授权成功");
                    final String userId = plat.getDb().getUserId();
                    Log.i("userId:" + userId);
                    final String userName = plat.getDb().getUserName();
                    Log.i("userName:" + userName);
                    final String userIcon = plat.getDb().getUserIcon();

                    Log.i("userIcon:" + userIcon);

                    String type = "qq";
                    if (name.equals(QQ.NAME)) {
                        type = "qq";
                    } else {
                        type = "wx";
                    }
                    Log.i("第三方登录 type：" + type);

                    final String finalType = type;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            third_login(finalType, userId, userName, userIcon);
                        }
                    });
                }
            }

            @Override
            public void onError(Platform platform, int i, Throwable throwable) {
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        CommToast.showToast(mContext, "授权失败");
//                    }
//                });
                Log.i(throwable.getMessage());
                Log.i("第三方登录 onError：" + throwable.getMessage());
                dismissLoadingDialog();
            }

            @Override
            public void onCancel(Platform platform, int i) {
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        CommToast.showToast(mContext, "取消授权");
//                    }
//                });
                Log.i("第三方登录 onCancel");
                dismissLoadingDialog();
            }
        });
        if (!plat.isClientValid()) {
            dismissLoadingDialog();
            //判断是否存在授权凭条的客户端，true是有客户端，false是无
            CommToast.showToast(mContext, "没有安装客户端");
            return;
        }
        if (plat.isAuthValid()) {
            dismissLoadingDialog();
            //判断是否已经存在授权状态，可以根据自己的登录逻辑设置
            CommToast.showToast(mContext, "已经授权过了");
            return;
        }
        //plat.authorize();	//要功能，不要数据
        plat.showUser(null);    //要数据不要功能，主要体现在不会重复出现授权界面
        showLoadingDialog("");
    }

    private void third_login(String type, String openid, String nickname, String avatar) {
        Log.i("第三方登录");
        showLoadingDialog("");
        API_LoginManager.third_login(mContext, type, openid, nickname, avatar, new OkHttpCallBack() {
            @Override
            public void onSuccess(String result) {
                dismissLoadingDialog();
                try {
                    RegistResponce registResponce = new Gson().fromJson(result, RegistResponce.class);
                    UserManager.saveToken(mContext, registResponce.access_token);
//                    CommToast.showToast(mContext, "登录成功");
                    finish();
                } catch (Exception e) {
//                    CommToast.showToast(mContext, "服务器错误");
                }
            }

            @Override
            public void onFailure(String result) {
                dismissLoadingDialog();
//                CommToast.showToast(mContext, "请求失败，请重试");
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

    }
}
