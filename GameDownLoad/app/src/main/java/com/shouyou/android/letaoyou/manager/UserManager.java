package com.shouyou.android.letaoyou.manager;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;


import com.shouyou.android.letaoyou.MyApplication;
import com.shouyou.android.letaoyou.bean.UserInfo;
import com.shouyou.android.letaoyou.constant.SPConstants;
import com.shouyou.android.letaoyou.event.Event_Logout;
import com.shouyou.android.letaoyou.tool.SPUtil;
import com.shouyou.android.letaoyou.ui.activity.LoginActivity;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by Administrator on 2018/8/29.
 */

public class UserManager {

    private static UserInfo userInfo;

    public static void saveUserInfo(Context context) {
        SPUtil.putObjectValue(context, SPConstants.UserInfo, userInfo);
    }

    //保存用户信息
    public static void saveUserInfo(Context context, UserInfo info) {
        SPUtil.putObjectValue(context, SPConstants.UserInfo, info);
        userInfo = info;
    }

    //获取用户信息
    public static UserInfo getUserInfo(Context context) {
        if (userInfo == null) {
            userInfo = (UserInfo) SPUtil.getObjectValue(context, UserInfo.class, SPConstants.UserInfo);
        }
        return userInfo;
    }

    //保存Token
    public static void saveToken(Context context, String Token) {
        SPUtil.putValue(context, SPConstants.Token, Token);
    }

    //获取Token
    public static String getToken(Context context) {
        return SPUtil.getStringValue(context, SPConstants.Token, "");
    }

    //是否登录
    public static boolean isLogin(Context context) {
        return !TextUtils.isEmpty(getToken(context));
    }

    //退出登录
    public static void logout(Context context) {
        userInfo = null;
        saveUserInfo(MyApplication.context);
        saveToken(context, "");
        EventBus.getDefault().post(new Event_Logout());
//        showLogin(context);
    }

    //跳登录页
    public static void showLogin(Context context) {
        Intent intent = new Intent(context, LoginActivity.class);
        context.startActivity(intent);
    }

}
