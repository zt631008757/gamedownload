package com.shouyou.android.letaoyou.ui.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;

import com.jwenfeng.library.pulltorefresh.PullToRefreshLayout;
import com.lzy.okgo.db.DownloadManager;
import com.lzy.okgo.model.Progress;
import com.lzy.okserver.OkDownload;
import com.lzy.okserver.download.DownloadTask;
import com.shouyou.android.letaoyou.R;
import com.shouyou.android.letaoyou.adapter.GameListAdapter;
import com.shouyou.android.letaoyou.bean.GameInfo;
import com.shouyou.android.letaoyou.ui.view.MultiStateView;
import com.shouyou.android.letaoyou.ui.view.swipemenulistview.SwipeMenu;
import com.shouyou.android.letaoyou.ui.view.swipemenulistview.SwipeMenuCreator;
import com.shouyou.android.letaoyou.ui.view.swipemenulistview.SwipeMenuItem;
import com.shouyou.android.letaoyou.ui.view.swipemenulistview.SwipeMenuListView;
import com.shouyou.android.letaoyou.util.Util;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Administrator on 2018/6/20.
 */

public class DownLoadListActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_download);
        initView();
        initSwipeList();
        getData();
    }

    MultiStateView multiplestatusView;
    RecyclerView recyclerview;
    GameListAdapter adapter;

    private void initView() {
        setTitle("下载中心");
        setLeftImgClickListener();

        multiplestatusView = (MultiStateView) findViewById(R.id.multiplestatusView);
        multiplestatusView.setViewState(MultiStateView.ViewState.LOADING);
        multiplestatusView.setOnRetryListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                multiplestatusView.setViewState(MultiStateView.ViewState.LOADING);
                getData();
            }
        });

        recyclerview = findViewById(R.id.recyclerview);
        recyclerview.setLayoutManager(new LinearLayoutManager(mContext));
        adapter = new GameListAdapter(mContext, null);
        recyclerview.setAdapter(adapter);
    }

    private void getData() {
        List<GameInfo> list = new ArrayList<>();

        List<DownloadTask> values = OkDownload.restore(DownloadManager.getInstance().getAll());

//        List<Progress> downloadList = DownloadManager.getInstance().getAll();
        for (int i = 0; i < values.size(); i++) {
            Progress progress = values.get(i).progress;
            GameInfo gameInfo = (GameInfo) progress.extra1;
            list.add(0, gameInfo);
        }

        adapter.setData(list);
        if (list.size() > 0) {
            multiplestatusView.setViewState(MultiStateView.ViewState.CONTENT);
        } else {
            multiplestatusView.setViewState(MultiStateView.ViewState.EMPTY);
        }
    }

    public void initSwipeList() {
        //为RecycleView绑定触摸事件
        ItemTouchHelper helper = new ItemTouchHelper(new ItemTouchHelper.Callback() {
            @Override
            public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
                //首先回调的方法 返回int表示是否监听该方向
                int dragFlags = ItemTouchHelper.UP | ItemTouchHelper.DOWN;//拖拽
                int swipeFlags = ItemTouchHelper.LEFT;//侧滑删除
                return makeMovementFlags(dragFlags, swipeFlags);
            }

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                //滑动事件
                return false;
            }

            @Override
            public void onSelectedChanged(RecyclerView.ViewHolder viewHolder, int actionState) {
                super.onSelectedChanged(viewHolder, actionState);
            }

            @Override
            public void onSwiped(final RecyclerView.ViewHolder viewHolder, int direction) {
                GameListAdapter.ContentViewHolder viewHolder1 = (GameListAdapter.ContentViewHolder) viewHolder;

                //侧滑事件
                OkDownload.getInstance().removeTask(viewHolder1.tag);
                getData();
            }

            @Override
            public boolean isLongPressDragEnabled() {
                //是否可拖拽
                return true;
            }
        });
        helper.attachToRecyclerView(recyclerview);

    }
}
