package com.shouyou.android.letaoyou.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.shouyou.android.letaoyou.R;
import com.shouyou.android.letaoyou.adapter.GameListAdapter;
import com.shouyou.android.letaoyou.adapter.Home_Search_HistoryAdapter;
import com.shouyou.android.letaoyou.constant.SPConstants;
import com.shouyou.android.letaoyou.interface_.CommCallBack;
import com.shouyou.android.letaoyou.interface_.OkHttpCallBack;
import com.shouyou.android.letaoyou.manager.API_LoginManager;
import com.shouyou.android.letaoyou.responce.GetGameListResponce;
import com.shouyou.android.letaoyou.tool.CommToast;
import com.shouyou.android.letaoyou.tool.SPUtil;
import com.shouyou.android.letaoyou.ui.view.MultiStateView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2018/6/20.
 */

public class SearchActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        initView();
    }

    TextView tv_cancel;
    EditText et_search;
    RecyclerView recyclerview_history, recyclerview;
    LinearLayout ll_history;
    ImageView iv_clearhistory;

    Home_Search_HistoryAdapter historyAdapter;
    GameListAdapter searchListAdapter;

    private void initView() {
        ll_history = findViewById(R.id.ll_history);

        //历史列表
        recyclerview_history = findViewById(R.id.recyclerview_history);
        recyclerview_history.setLayoutManager(new LinearLayoutManager(mContext));
        historyAdapter = new Home_Search_HistoryAdapter(mContext, new CommCallBack() {
            @Override
            public void onResult(Object obj) {
                String text = (String) obj;
                if (TextUtils.isEmpty(text)) {
                    historyAdapter.setData(getHistory());
                } else {
                    search(text);
                }
            }
        });
        recyclerview_history.setAdapter(historyAdapter);
        historyAdapter.setData(getHistory());



        //搜索结果
        recyclerview = findViewById(R.id.recyclerview);
        recyclerview.setLayoutManager(new LinearLayoutManager(mContext));
        searchListAdapter = new GameListAdapter(mContext, null);
        recyclerview.setAdapter(searchListAdapter);


        tv_cancel = findViewById(R.id.tv_cancel);
        iv_clearhistory = findViewById(R.id.iv_clearhistory);
        tv_cancel.setOnClickListener(this);
        iv_clearhistory.setOnClickListener(this);

        et_search = findViewById(R.id.et_search);
        et_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH || (event.getKeyCode() == KeyEvent.KEYCODE_ENTER && event.getAction() == KeyEvent.ACTION_DOWN)) {
                    String s = et_search.getText().toString().trim();
                    if (s.length() > 0) {
                        search(s.toString());
//                        ll_history.setVisibility(View.GONE);
                    } else {
//                        CommToast.showToast(mContext, "请输入手机号");
//                        ll_history.setVisibility(View.VISIBLE);
                    }
                    return true;
                } else {
                    return false;
                }
            }
        });
        et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() >= 6) {
                    search(s.toString());
                } else {
                    recyclerview.setVisibility(View.GONE);
                    ll_history.setVisibility(View.VISIBLE);
                }
            }
        });
    }


    private void search(String phone) {
        if (TextUtils.isEmpty(phone)) {
            CommToast.showToast(mContext, "请输入游戏名称");
            return;
        }
        //存入本地
        putHistory(phone);


        API_LoginManager.games(mContext, "", phone,"0","","", new OkHttpCallBack() {
            @Override
            public void onSuccess(String result) {
                GetGameListResponce responce = new Gson().fromJson(result, GetGameListResponce.class);
                searchListAdapter.setData(responce.data);
                recyclerview.setVisibility(View.VISIBLE);
                ll_history.setVisibility(View.GONE);

                if(responce==null||responce.data==null||responce.data.size()==0)
                {
                    CommToast.showToast(mContext,"无搜索结果");
//                    multiplestatusView.setViewState(MultiStateView.ViewState.EMPTY);
                }
            }

            @Override
            public void onFailure(String result) {
//                pulltorefreshlayout.finishRefresh();
//                pulltorefreshlayout.finishLoadMore();
//                multiplestatusView.setViewState(MultiStateView.ViewState.EMPTY);
            }
        });


    }

    private void putHistory(String text) {
        List<String> searchList = getHistory();
        if (searchList.contains(text)) return;
        searchList.add(text);
        SPUtil.putValue(mContext, SPConstants.HOME_SEARCH_HISTORY, new Gson().toJson(searchList));
        historyAdapter.setData(getHistory());
    }

    private List<String> getHistory() {
        String listStr = SPUtil.getStringValue(mContext, SPConstants.HOME_SEARCH_HISTORY, "");
        List<String> searchList = new Gson().fromJson(listStr, new TypeToken<List<String>>() {
        }.getType());
        if (searchList == null) searchList = new ArrayList<>();
//        if (searchList.size() == 0) {
//            ll_history.setVisibility(View.GONE);
//        } else {
//            ll_history.setVisibility(View.VISIBLE);
//        }
        return searchList;
    }

    private void clearHis(String text) {
        if (TextUtils.isEmpty(text)) {
            SPUtil.putValue(mContext, SPConstants.HOME_SEARCH_HISTORY, "");
        } else {
            List<String> searchList = getHistory();
            searchList.remove(text);
            SPUtil.putValue(mContext, SPConstants.HOME_SEARCH_HISTORY, new Gson().toJson(searchList));
        }
        historyAdapter.setData(getHistory());
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.tv_cancel:
                finish();
                break;
            case R.id.iv_clearhistory:
                clearHis("");
                break;
        }
    }
}
