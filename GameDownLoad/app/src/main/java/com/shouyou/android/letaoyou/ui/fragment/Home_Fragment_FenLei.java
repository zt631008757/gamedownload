package com.shouyou.android.letaoyou.ui.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.shouyou.android.letaoyou.R;
import com.shouyou.android.letaoyou.adapter.FenLei_TagAdapter;
import com.shouyou.android.letaoyou.bean.TagInfo;
import com.shouyou.android.letaoyou.constant.SPConstants;
import com.shouyou.android.letaoyou.interface_.CommCallBack;
import com.shouyou.android.letaoyou.interface_.OkHttpCallBack;
import com.shouyou.android.letaoyou.manager.API_LoginManager;
import com.shouyou.android.letaoyou.tool.CommToast;
import com.shouyou.android.letaoyou.tool.SPUtil;
import com.shouyou.android.letaoyou.ui.activity.BenZhouXinYouActivity;
import com.shouyou.android.letaoyou.ui.activity.FenLei_BTGameActivity;
import com.shouyou.android.letaoyou.ui.activity.FenLei_H5GameActivity;
import com.shouyou.android.letaoyou.ui.activity.GameListActivity;
import com.shouyou.android.letaoyou.ui.activity.WebViewActivity;
import com.shouyou.android.letaoyou.ui.view.MultiStateView;

import java.util.List;

/**
 * Created by Administrator on 2018/8/20.
 * 分类
 */

public class Home_Fragment_FenLei extends BaseFragment {

    private static Home_Fragment_FenLei fragment = null;

    public static Home_Fragment_FenLei newInstance() {
        if (fragment == null) {
            fragment = new Home_Fragment_FenLei();
        }
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mContext = getActivity();
        rootView = inflater.inflate(R.layout.fragment_home_fenlei, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
        getData();
    }

    Context mContext;
    View rootView;
    RecyclerView recyclerview;
    MultiStateView multiplestatusView;
    LinearLayout ll_benzhouxinyou, ll_zhibo, ll_btgame, ll_xiaoyouxi;
    SwipeRefreshLayout refreshlayout;

    FenLei_TagAdapter tagAdapter;
    List<TagInfo> tagInfoList;

    private void initView() {
        multiplestatusView = rootView.findViewById(R.id.multiplestatusView);
        multiplestatusView.setViewState(MultiStateView.ViewState.LOADING);
        multiplestatusView.setOnRetryListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                multiplestatusView.setViewState(MultiStateView.ViewState.LOADING);
                getData();
            }
        });


        recyclerview = rootView.findViewById(R.id.recyclerview);
        LinearLayoutManager lm = new LinearLayoutManager(mContext);
        recyclerview.setLayoutManager(lm);
        tagAdapter = new FenLei_TagAdapter(mContext, callBack);
        recyclerview.setAdapter(tagAdapter);
        recyclerview.setHasFixedSize(true);
        recyclerview.setNestedScrollingEnabled(false);

        refreshlayout = rootView.findViewById(R.id.refreshlayout);
        refreshlayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getData();
            }
        });

        ll_benzhouxinyou = rootView.findViewById(R.id.ll_benzhouxinyou);
        ll_zhibo = rootView.findViewById(R.id.ll_zhibo);
        ll_btgame = rootView.findViewById(R.id.ll_btgame);
        ll_xiaoyouxi = rootView.findViewById(R.id.ll_xiaoyouxi);

        ll_benzhouxinyou.setOnClickListener(this);
        ll_zhibo.setOnClickListener(this);
        ll_btgame.setOnClickListener(this);
        ll_xiaoyouxi.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        Intent intent;
        switch (v.getId()) {
            case R.id.ll_benzhouxinyou:  //本周新游
                intent = new Intent(mContext, BenZhouXinYouActivity.class);
                startActivity(intent);
                break;
            case R.id.ll_zhibo:  //公会直播
                String url = SPUtil.getStringValue(mContext, SPConstants.Live_url, "");
                if (!TextUtils.isEmpty("url")) {
                    intent = new Intent(mContext, WebViewActivity.class);
                    intent.putExtra("url", url);
                    intent.putExtra("title", "公会直播");
                    startActivity(intent);
                } else {
                    CommToast.showToast(mContext, "暂无链接");
                }

                break;
            case R.id.ll_btgame:   //BT游戏
                intent = new Intent(mContext, FenLei_BTGameActivity.class);
                startActivity(intent);
                break;
            case R.id.ll_xiaoyouxi:   //H5小游戏
                intent = new Intent(mContext, FenLei_H5GameActivity.class);
                startActivity(intent);
                break;
        }
    }

    CommCallBack callBack = new CommCallBack() {
        @Override
        public void onResult(Object obj) {
            String id = (String) obj;
            int index = 0;
            for (int i = 0; i < tagInfoList.size(); i++) {
                if (tagInfoList.get(i).id.equals(id)) {
                    index = i;
                }
            }

            Intent intent = new Intent(mContext, GameListActivity.class);
            intent.putExtra("tagList", new Gson().toJson(tagInfoList));
            intent.putExtra("index", index);
            startActivity(intent);

        }
    };

    private void getData() {
        API_LoginManager.games_type_list(mContext, new OkHttpCallBack() {
            @Override
            public void onSuccess(String baseResponce) {
                refreshlayout.setRefreshing(false);
                multiplestatusView.setViewState(MultiStateView.ViewState.CONTENT);

                try {
                    tagInfoList = new Gson().fromJson(baseResponce, new TypeToken<List<TagInfo>>() {
                    }.getType());

                    bindUI();
                } catch (Exception e) {
                    multiplestatusView.setViewState(MultiStateView.ViewState.ERROR);
                }
            }

            @Override
            public void onFailure(String baseResponce) {
                refreshlayout.setRefreshing(false);
                multiplestatusView.setViewState(MultiStateView.ViewState.ERROR);
            }
        });
    }

    private void bindUI() {
        tagAdapter.setData(tagInfoList);

    }

}
