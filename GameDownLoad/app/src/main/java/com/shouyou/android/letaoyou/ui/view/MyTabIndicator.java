package com.shouyou.android.letaoyou.ui.view;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.shouyou.android.letaoyou.R;
import com.shouyou.android.letaoyou.util.Util;

import java.util.ArrayList;
import java.util.List;

/**
 * tab切换
 */
public class MyTabIndicator extends LinearLayout {

    Context context;
    List<RelativeLayout> childViews = new ArrayList<>();
    RelativeLayout preSelectView = null;
    OnTabChangedListener onTabChangedListener;
    int currentSelectIndex = 0;
    String selectTextColor= "#FC203F";
    String normalTextColor= "#484848";
    String indicatorColor= "#FC203F";  //滑块颜色
    int viewWidth = 30 ;   //滑块宽度

    public MyTabIndicator(Context context) {
        super(context);
        this.context = context;
        initView();
    }

    public MyTabIndicator(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        initView();
    }


    View rootView;
    LinearLayout ll_container;
    View view_selected_new;

    private void initView() {
        viewWidth = Util.dip2px(context,20);

        rootView = View.inflate(context, R.layout.widget_mytabindicator, this);
        ll_container = (LinearLayout) rootView.findViewById(R.id.ll_container);
        view_selected_new = rootView.findViewById(R.id.view_selected_new);
//        view_selected_new.setBackgroundColor(Color.parseColor(indicatorColor));
    }

    public interface OnTabChangedListener {
        void onTabChanged(int index);
    }

    //绑定数据
    public void setData(List<String> strings, boolean isFullScreen, List<String>... nums) {
        currentSelectIndex = 0;
        childViews.clear();
        ll_container.removeAllViews();
        for (int i = 0; i < strings.size(); i++) {
            RelativeLayout relativeLayout;
            if (isFullScreen) {
                relativeLayout = (RelativeLayout) View.inflate(context, R.layout.widget_tab_fullscreen, null);
                LayoutParams params = new LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT);
                params.weight = 1;
                relativeLayout.setLayoutParams(params);
            } else {
                relativeLayout = (RelativeLayout) View.inflate(context, R.layout.widget_tab_fullscreen, null);
                relativeLayout.setPadding(Util.dip2px(context, 15), 0, Util.dip2px(context, 15), 0);
            }
            //文字
            TextView title = (TextView) relativeLayout.findViewById(R.id.title);
            title.setText(strings.get(i));
            title.setTextColor(Color.parseColor(normalTextColor));
            //角标
            TextView count = (TextView) relativeLayout.findViewById(R.id.count);
            if (nums.length > 0) {
                if (!TextUtils.isEmpty(nums[0].get(i))) {
                    count.setVisibility(VISIBLE);
                    count.setText(nums[0].get(i));
                } else {
                    count.setVisibility(GONE);
                }
            }


            final int finalI = i;
            relativeLayout.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    currentSelectIndex = finalI;
                    if (onTabChangedListener != null) {
                        onTabChangedListener.onTabChanged(finalI);
                    }
                    setSelectedPosition(finalI);
                }
            });
            childViews.add(relativeLayout);
            ll_container.addView(relativeLayout);
        }

    }


    /**
     * 设置tab角标
     *
     * @param nums
     */
    public void setNums(List<String> nums) {
        for (int i = 0; i < nums.size(); i++) {
            TextView textViewNum = (TextView) childViews.get(i).findViewById(R.id.count);
            if (!TextUtils.isEmpty(nums.get(i))) {
                textViewNum.setVisibility(VISIBLE);
                textViewNum.setText(nums.get(i));
            } else {
                textViewNum.setVisibility(GONE);
            }
        }
    }


    //绑定点击事件
    public void setOnTabChangedListener(OnTabChangedListener onTabChangedListener) {
        this.onTabChangedListener = onTabChangedListener;
    }

    //外部切换
    public void setSelectedPosition(int index) {
        if (preSelectView != null) {
            TextView title = (TextView) preSelectView.findViewById(R.id.title);
            title.setTextColor(Color.parseColor(normalTextColor));
//            preSelectView.findViewById(R.id.line).setVisibility(GONE);
        }
        RelativeLayout relativeLayout = childViews.get(index);
        TextView title = (TextView) relativeLayout.findViewById(R.id.title);
        title.setTextColor(getResources().getColor(R.color.mainColor));
//        relativeLayout.findViewById(R.id.line).setVisibility(VISIBLE);
        preSelectView = relativeLayout;
    }

    public void refresh() {
        if (onTabChangedListener != null) {
            onTabChangedListener.onTabChanged(currentSelectIndex);
        }
    }

    /**
     * viewpager滚动时 调用此方法
     *
     * @param i
     * @param v
     */
    public void setOnscroll(int i, float v) {
        if (childViews.size() == 0) return;
        RelativeLayout relativeLayout = childViews.get(i);
        TextView title = (TextView) relativeLayout.findViewById(R.id.title);
        float del = 0;
        float left = 0;
        float width = 0;
        if (i == childViews.size() - 1) {
            left = relativeLayout.getLeft() + title.getLeft() + (title.getWidth() - viewWidth) / 2;
            width = title.getWidth();
        } else {
            del = (relativeLayout.getWidth() + (childViews.get(i + 1).findViewById(R.id.title).getLeft() +
                    (childViews.get(i + 1).findViewById(R.id.title).getWidth() - viewWidth) / 2
                    - (title.getWidth() - viewWidth) / 2
                    - title.getLeft())) * v;
            left = relativeLayout.getLeft() + title.getLeft() + del
                    + (title.getWidth() - viewWidth) / 2
            ;
            width = title.getWidth() + (childViews.get(i + 1).findViewById(R.id.title).getWidth() - title.getWidth()) * v;
        }
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(viewWidth, Util.dip2px(getContext(), 3));
        params.setMargins((int) left, 0, 0, Util.dip2px(getContext(), 5));
        view_selected_new.setLayoutParams(params);
    }

}
