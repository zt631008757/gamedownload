package com.shouyou.android.letaoyou.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.shouyou.android.letaoyou.R;
import com.shouyou.android.letaoyou.adapter.FragmentViewPagerAdapter;
import com.shouyou.android.letaoyou.ui.view.MyTabIndicator;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2018/8/20.
 * 发现
 */

public class Home_Fragment_FaXian extends BaseFragment {

    private static Home_Fragment_FaXian fragment = null;

    public static Home_Fragment_FaXian newInstance() {
        if (fragment == null) {
            fragment = new Home_Fragment_FaXian();
        }
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mContext = getActivity();
        rootView = inflater.inflate(R.layout.fragment_home_faxian, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
    }

    Context mContext;
    View rootView;
    MyTabIndicator mytabindicator;
    ViewPager viewpager;


    private void initView() {
        mytabindicator = rootView.findViewById(R.id.mytabindicator);
        viewpager = rootView.findViewById(R.id.viewpager);

        List<String> list = new ArrayList<>();
        List<Fragment> fragmentList = new ArrayList<>();

//        list.add("全部");
        list.add("推荐");
        list.add("看点");
        list.add("活动");
        list.add("资讯");
        mytabindicator.setData(list, true);

        for (int i = 1; i < list.size()+1; i++) {
            FaXian_ContentFragment fragment = new FaXian_ContentFragment();
            fragment.type = i + "";
            fragmentList.add(fragment);
        }

        FragmentViewPagerAdapter viewPagerAdapter = new FragmentViewPagerAdapter(getChildFragmentManager(), fragmentList == null ? null : fragmentList);
        viewpager.setAdapter(viewPagerAdapter);
        viewpager.setOffscreenPageLimit(list.size());
        viewpager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                mytabindicator.setSelectedPosition(position);
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
                mytabindicator.setOnscroll(arg0, arg1);
            }

            @Override
            public void onPageScrollStateChanged(int arg0) {
            }
        });
        //点击tabhost中的tab时，要切换下面的viewPager
        mytabindicator.setOnTabChangedListener(new MyTabIndicator.OnTabChangedListener() {
            @Override
            public void onTabChanged(int index) {
                viewpager.setCurrentItem(index);
            }
        });

//        mytabindicator.setSelect(index);
//        final int finalIndex = index;
//        handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                mytabindicator.setSelectedPosition(finalIndex);
//            }
//        }, 500);

    }


}
