package com.shouyou.android.letaoyou.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.shouyou.android.letaoyou.R;
import com.shouyou.android.letaoyou.bean.GameInfo;
import com.shouyou.android.letaoyou.interface_.CommCallBack;
import com.shouyou.android.letaoyou.ui.activity.H5GameDetailActivity;
import com.shouyou.android.letaoyou.util.GlideUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2018/8/22.
 */

public class H5GameAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context mContext;
    List<GameInfo> list = new ArrayList<>();
    CommCallBack callBack;

    public H5GameAdapter(Context mContext, CommCallBack callBack) {
        this.mContext = mContext;
        this.callBack = callBack;
    }

    public void setData(List<GameInfo> list) {
        this.list=list;
        notifyDataSetChanged();
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        ViewGroup viewGroup = (ViewGroup) mInflater.inflate(R.layout.item_h5gamelist, null, false);
        ContentViewHolder holder = new ContentViewHolder(viewGroup);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final ContentViewHolder viewHolder = (ContentViewHolder) holder;
        final GameInfo info = list.get(position);
        final String tagStr = info.id + "_" + info.name;

        //游戏部分
        GlideUtil.displayImage(mContext, info.logo, viewHolder.iv_gamelogo, -1);
        viewHolder.tv_game_name.setText(info.name);
        viewHolder.tv_size.setText(info.size + "M  |  ");
        viewHolder.tv_subname.setText(info.subtitle);

        viewHolder.tv_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent  intent = new Intent(mContext, H5GameDetailActivity.class);
                intent.putExtra("url",info.download_link);
                intent.putExtra("title","小游戏");
                mContext.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }


    class ContentViewHolder extends RecyclerView.ViewHolder {

        ImageView iv_gamelogo;
        TextView tv_game_name, tv_size, tv_tag, tv_subname,tv_play;

        public ContentViewHolder(View itemView) {
            super(itemView);
            iv_gamelogo = itemView.findViewById(R.id.iv_gamelogo);
            tv_game_name = itemView.findViewById(R.id.tv_game_name);
            tv_size = itemView.findViewById(R.id.tv_size);
            tv_tag = itemView.findViewById(R.id.tv_tag);
            tv_subname = itemView.findViewById(R.id.tv_subname);
            tv_play= itemView.findViewById(R.id.tv_play);
        }
    }
}
