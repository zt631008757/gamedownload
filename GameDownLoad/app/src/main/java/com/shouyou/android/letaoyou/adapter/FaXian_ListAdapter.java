package com.shouyou.android.letaoyou.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.shouyou.android.letaoyou.R;
import com.shouyou.android.letaoyou.bean.WenZhangInfo;
import com.shouyou.android.letaoyou.interface_.CommCallBack;
import com.shouyou.android.letaoyou.ui.activity.FaXianDetailActivity;
import com.shouyou.android.letaoyou.util.GlideUtil;
import com.shouyou.android.letaoyou.util.Util;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2018/8/22.
 */

public class FaXian_ListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context mContext;
    List<WenZhangInfo> list = new ArrayList<>();
    CommCallBack callBack;
    int width = 0;

    public FaXian_ListAdapter(Context mContext, CommCallBack callBack) {
        this.mContext = mContext;
        this.callBack = callBack;
        width = mContext.getResources().getDisplayMetrics().widthPixels - Util.dip2px(mContext, 40);
    }

    public void setData(List<WenZhangInfo> list) {
        this.list.clear();
        this.list.addAll(list);
        notifyDataSetChanged();
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        ViewGroup viewGroup = (ViewGroup) mInflater.inflate(R.layout.item_faxian_list, null, false);
        ContentViewHolder holder = new ContentViewHolder(viewGroup);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ContentViewHolder viewHolder = (ContentViewHolder) holder;
        final WenZhangInfo info = list.get(position);

        GlideUtil.displayImage(mContext, info.picture, viewHolder.iv_img, R.drawable.ico_default);
        viewHolder.tv_title.setText(info.title);
        viewHolder.tv_time.setText(info.updated_at);

        if(position==0)
        {
            viewHolder.view_space.setVisibility(View.VISIBLE);
        }
        else
        {
            viewHolder.view_space.setVisibility(View.GONE);
        }


        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, FaXianDetailActivity.class);
                intent.putExtra("id", info.id);
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }


    class ContentViewHolder extends RecyclerView.ViewHolder {
        TextView tv_title, tv_time;
        ImageView iv_img;
        View view_space;

        public ContentViewHolder(View itemView) {
            super(itemView);
            tv_title = itemView.findViewById(R.id.tv_title);
            iv_img = itemView.findViewById(R.id.iv_img);
            tv_time = itemView.findViewById(R.id.tv_time);
            view_space = itemView.findViewById(R.id.view_space);


            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(width, width / 2);
            iv_img.setLayoutParams(params);

        }
    }
}
