package com.shouyou.android.letaoyou.ui.fragment;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.view.View;

import java.util.List;

/**
 * Created by zt on 2018/6/12.
 */
public class BaseFragment extends Fragment implements View.OnClickListener{

    @Override
    public void startActivityForResult(Intent intent, int requestCode) {
        Fragment fragment = getParentFragment();
        if (fragment != null) {
            fragment.startActivityForResult(intent, requestCode);
        } else {
            super.startActivityForResult(intent, requestCode);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        List<Fragment> fragmentList = getChildFragmentManager().getFragments();
        if (fragmentList != null) {
            for(Fragment fragment : fragmentList){
                if(fragment != null) {
                    fragment.onActivityResult(requestCode, resultCode, data);
                }
            }
        }
    }

    @Override
    public void onClick(View v) {

    }
}
