package com.shouyou.android.letaoyou.adapter;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.List;

/**
 * Created by zt on 2015/12/21.
 *  viewpager填充器
 */
public class FragmentViewPagerAdapter extends FragmentStatePagerAdapter {
    List<Fragment> fragmentsList;
    public FragmentViewPagerAdapter(FragmentManager fm, List<Fragment> fragmentsList) {
        super(fm);
        this.fragmentsList = fragmentsList;
    }

    @Override
    public int getCount() {
        return fragmentsList.size();
    }

    @Override
    public Fragment getItem(int i) {
        return fragmentsList.get(i);
    }

    public int getItemPosition(Object object){
        return POSITION_NONE;
    }
}
