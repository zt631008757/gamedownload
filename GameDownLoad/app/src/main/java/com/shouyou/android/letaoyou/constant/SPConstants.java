package com.shouyou.android.letaoyou.constant;

/**
 * Created by ZT on 2018/8/13.
 * SPUtil key 常量
 */

public class SPConstants {
    //使用相关
    public static final String LaunchTIMES = "LaunchTIMES";   //启动次数

    public static final String UserInfo = "UserInfo";   //用户信息
    public static final String Token = "Token";   //用户信息

    public static final String Live_url = "live_url";   //直播链接
    public static final String Guild_url = "guild_url";   //公会周边

    public static final String HOME_SEARCH_HISTORY = "HOME_SEARCH_HISTORY";   //首页搜索记录

}
