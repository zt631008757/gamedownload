package com.shouyou.android.letaoyou.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.shouyou.android.letaoyou.R;
import com.shouyou.android.letaoyou.interface_.OkHttpCallBack;
import com.shouyou.android.letaoyou.manager.API_LoginManager;
import com.shouyou.android.letaoyou.manager.UserManager;
import com.shouyou.android.letaoyou.responce.BaseResponce;
import com.shouyou.android.letaoyou.responce.RegistResponce;
import com.shouyou.android.letaoyou.tool.CommToast;

/**
 * Created by Administrator on 2018/6/20.
 */

public class RegistActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regist);
        initView();
    }

    EditText et_account, et_pwd, et_pwd1;
    TextView tv_regist,tv_tologin;

    private void initView() {
        setTitle("注册");
        setLeftImgClickListener();

        et_account = findViewById(R.id.et_account);
        et_pwd = findViewById(R.id.et_pwd);
        et_pwd1 = findViewById(R.id.et_pwd1);
        tv_regist = findViewById(R.id.tv_regist);
        tv_tologin= findViewById(R.id.tv_tologin);

        tv_regist.setOnClickListener(this);
        tv_tologin.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.tv_regist:
                regist();
                break;
            case R.id.tv_tologin:
                Intent intent = new Intent(mContext, LoginActivity.class);
                startActivity(intent);
                finish();
                break;
        }
    }


    private void regist() {
        String account = et_account.getText().toString().trim();
        String pwd = et_pwd.getText().toString().trim();
        String pwd1 = et_pwd1.getText().toString().trim();
        if (TextUtils.isEmpty(account)) {
            CommToast.showToast(mContext, "请输入账号");
            return;
        }
        if (TextUtils.isEmpty(pwd)) {
            CommToast.showToast(mContext, "请输入密码");
            return;
        }

        API_LoginManager.register(mContext, account, pwd, pwd1, new OkHttpCallBack() {
            @Override
            public void onSuccess(String baseResponce) {
                RegistResponce registResponce = new Gson().fromJson(baseResponce, RegistResponce.class);
                UserManager.saveToken(mContext, registResponce.access_token);
                finish();
            }

            @Override
            public void onFailure(String baseResponce) {

            }
        });
    }
}
