package com.shouyou.android.letaoyou.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.shouyou.android.letaoyou.R;
import com.shouyou.android.letaoyou.adapter.FenLei_TagAdapter;
import com.shouyou.android.letaoyou.adapter.GiftListAdapter;
import com.shouyou.android.letaoyou.bean.GameInfo;
import com.shouyou.android.letaoyou.bean.GiftInfo;
import com.shouyou.android.letaoyou.bean.TagInfo;
import com.shouyou.android.letaoyou.interface_.CommCallBack;
import com.shouyou.android.letaoyou.interface_.OkHttpCallBack;
import com.shouyou.android.letaoyou.manager.API_LoginManager;
import com.shouyou.android.letaoyou.tool.CommToast;
import com.shouyou.android.letaoyou.ui.view.MultiStateView;
import com.shouyou.android.letaoyou.util.Util;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by Administrator on 2018/8/20.
 */

public class GameDetail_FuliFragment extends BaseFragment {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mContext = getActivity();
        rootView = inflater.inflate(R.layout.fragment_gamedetail_fuli, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
        getData();
    }

    Context mContext;
    GameInfo gameInfo;

    View rootView;
    TextView tv_text;
    RecyclerView recyclerview;
    MultiStateView multiplestatusView;

    GiftListAdapter giftListAdapter;

    private void initView() {
        multiplestatusView = rootView.findViewById(R.id.multiplestatusView);
        multiplestatusView.setViewState(MultiStateView.ViewState.LOADING);
        multiplestatusView.setOnRetryListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                multiplestatusView.setViewState(MultiStateView.ViewState.LOADING);
                getData();
            }
        });

        tv_text = rootView.findViewById(R.id.tv_text);
        recyclerview = rootView.findViewById(R.id.recyclerview);
        LinearLayoutManager lm = new LinearLayoutManager(mContext);
        recyclerview.setLayoutManager(lm);
        giftListAdapter = new GiftListAdapter(mContext, onItemClick);
        recyclerview.setAdapter(giftListAdapter);
    }

    public void setData(GameInfo gameInfo) {
        this.gameInfo = gameInfo;
    }


    private void getData() {
        API_LoginManager.gifts_list(mContext, gameInfo.id, new OkHttpCallBack() {
            @Override
            public void onSuccess(String result) {
                multiplestatusView.setViewState(MultiStateView.ViewState.CONTENT);
                try {

                    List<GiftInfo> list = new Gson().fromJson(result, new TypeToken<List<GiftInfo>>() {
                    }.getType());
                    giftListAdapter.setData(list);
                    if (list == null || list.size() == 0) {
                        multiplestatusView.setViewState(MultiStateView.ViewState.EMPTY);
                    }
                } catch (Exception e) {

                }
            }

            @Override
            public void onFailure(String result) {
                multiplestatusView.setViewState(MultiStateView.ViewState.EMPTY);
            }
        });
    }

    CommCallBack onItemClick = new CommCallBack() {
        @Override
        public void onResult(Object obj) {
            String id = (String) obj;
            get_code(id);
        }
    };

    private void get_code(String giftId) {
        API_LoginManager.get_code(mContext, giftId, new OkHttpCallBack() {
            @Override
            public void onSuccess(String result) {
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    String msg = jsonObject.getString("msg");
                    CommToast.showToast(mContext, msg);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(String result) {

            }
        });
    }
}
