package com.shouyou.android.letaoyou.ui.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import com.shouyou.android.letaoyou.R;
import com.shouyou.android.letaoyou.interface_.OkHttpCallBack;
import com.shouyou.android.letaoyou.manager.API_LoginManager;
import com.shouyou.android.letaoyou.tool.CommToast;
import com.shouyou.android.letaoyou.tool.Log;
import com.shouyou.android.letaoyou.ui.view.MultiStateView;
import com.shouyou.android.letaoyou.ui.view.MyRatingBar;
import com.shouyou.android.letaoyou.util.Util;

/**
 * Created by Administrator on 2018/6/20.
 */

public class PingLunActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        gameId = getIntent().getStringExtra("gameId");
        setContentView(R.layout.activity_pinglun);
        initView();
        getData();
    }

    String gameId;
    MultiStateView multiplestatusView;
    EditText et_content;
    MyRatingBar myratingbar;

    private void initView() {
        setTitle("评分");
        setLeftImgClickListener();
        setRightTextClickListener("提交");

        multiplestatusView = (MultiStateView) findViewById(R.id.multiplestatusView);
//        multiplestatusView.setViewState(MultiStateView.ViewState.LOADING);
        multiplestatusView.setOnRetryListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                multiplestatusView.setViewState(MultiStateView.ViewState.LOADING);
                getData();
            }
        });

        et_content = findViewById(R.id.et_content);
        myratingbar = findViewById(R.id.myratingbar);
    }

    private void getData() {

    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.tv_right_text:
                saveComm();
                break;
        }
    }

    private void saveComm() {
        int score = (int) myratingbar.starStep;
        String text = et_content.getText().toString().trim();
        if (score == 0) {
            CommToast.showToast(mContext, "请打分");
            return;
        }

        API_LoginManager.save_comments(mContext, gameId, "", score + "", text, new OkHttpCallBack() {
            @Override
            public void onSuccess(String result) {
                Log.i("result:"+result);
                if("\"success\"".equals(result))
                {
                    CommToast.showToast(mContext, "评论成功");
                    finish();
                }
                else {
                    CommToast.showToast(mContext, Util.unicodeToCn(result));
                }
            }

            @Override
            public void onFailure(String result) {

            }
        });
    }
}
