package com.shouyou.android.letaoyou.ui.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.baidu.ocr.sdk.utils.ImageUtil;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.mob.tools.utils.FileUtils;
import com.shouyou.android.letaoyou.R;
import com.shouyou.android.letaoyou.bean.UserInfo;
import com.shouyou.android.letaoyou.interface_.CommCallBack;
import com.shouyou.android.letaoyou.interface_.OkHttpCallBack;
import com.shouyou.android.letaoyou.manager.API_LoginManager;
import com.shouyou.android.letaoyou.manager.UserManager;
import com.shouyou.android.letaoyou.tool.CommToast;
import com.shouyou.android.letaoyou.ui.view.MultiStateView;
import com.shouyou.android.letaoyou.util.GlideUtil;
import com.shouyou.android.letaoyou.util.Util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * Created by Administrator on 2018/6/20.
 */

public class UserInfoActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_userinfo);
        initView();

    }

    @Override
    protected void onResume() {
        super.onResume();
        getUserInfo();
    }

    MultiStateView multiplestatusView;
    ImageView iv_head;
    TextView et_name;
    LinearLayout ll_head, ll_nick;
    TextView tv_save;

    File file;
    String path = "";

    private void initView() {
        setTitle("个人资料");
        setLeftImgClickListener();

        multiplestatusView = (MultiStateView) findViewById(R.id.multiplestatusView);
        multiplestatusView.setViewState(MultiStateView.ViewState.LOADING);
        multiplestatusView.setOnRetryListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                multiplestatusView.setViewState(MultiStateView.ViewState.LOADING);
                getUserInfo();
            }
        });
        iv_head = findViewById(R.id.iv_head);
        et_name = findViewById(R.id.et_name);
        ll_head = findViewById(R.id.ll_head);
        tv_save = findViewById(R.id.tv_save);
        ll_nick = findViewById(R.id.ll_nick);

        ll_head.setOnClickListener(this);
        tv_save.setOnClickListener(this);
        ll_nick.setOnClickListener(this);

        path = Util.getDiskCacheRootDir(mContext) + "/temp.jpg";
    }

    //获取用户信息
    public void getUserInfo() {
        API_LoginManager.userinfo(mContext, new OkHttpCallBack() {
            @Override
            public void onSuccess(String baseResponce) {
                multiplestatusView.setViewState(MultiStateView.ViewState.CONTENT);
                try {
                    UserInfo userInfo = new Gson().fromJson(baseResponce, UserInfo.class);
                    UserManager.saveUserInfo(mContext, userInfo);
                    bindUI();
                } catch (Exception e) {
                    multiplestatusView.setViewState(MultiStateView.ViewState.ERROR);
                }
            }

            @Override
            public void onFailure(String baseResponce) {
                multiplestatusView.setViewState(MultiStateView.ViewState.ERROR);
            }
        });
    }

    private void bindUI() {
        UserInfo userInfo = UserManager.getUserInfo(mContext);
        et_name.setText(userInfo.nickname);
        GlideUtil.displayImage(mContext, userInfo.avatar, iv_head, R.drawable.ico_default_head_fang);
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.ll_head:
                upLoadPhoto(true, new CommCallBack() {
                    @Override
                    public void onResult(Object obj) {
                        File fileTemp = (File) obj;
                        save(fileTemp);
//
//                        try {
//                            Bitmap bitmap = decodeFile(fileTemp.getPath());
//                            ImageUtil.resize(fileTemp.getPath(), path, 300, 300);
//                            GlideUtil.displayImage(mContext, bitmap, iv_head, R.drawable.ico_default_head_fang);
//
//                            file = new File(path);
//                        } catch (IOException e) {
//                            e.printStackTrace();
//                        }
                    }
                });
                break;
            case R.id.tv_save:
//                save();
                break;
            case R.id.ll_nick:
                Intent intent = new Intent(mContext, NickNameActivity.class);
                startActivity(intent);
                break;
        }
    }

    private void save(File file) {
        showLoadingDialog("正在上传");
        API_LoginManager.setting(mContext, null, file, new OkHttpCallBack() {
            @Override
            public void onSuccess(String result) {
                dismissLoadingDialog();
                try {
                    if ("\"success\"".equals(result)) {
                        CommToast.showToast(mContext, "修改成功");
                        getUserInfo();
                    } else {
                        CommToast.showToast(mContext, Util.unicodeToCn(result));
                    }
                } catch (Exception e) {

                }
            }

            @Override
            public void onFailure(String result) {
                dismissLoadingDialog();
            }
        });
    }

    /**
     * 根据 路径 得到 file 得到 bitmap
     *
     * @param filePath
     * @return
     * @throws IOException
     */
    public static Bitmap decodeFile(String filePath) throws IOException {
        Bitmap b = null;
        int IMAGE_MAX_SIZE = 600;

        File f = new File(filePath);
        if (f == null) {
            return null;
        }
        //Decode image size
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;

        FileInputStream fis = new FileInputStream(f);
        BitmapFactory.decodeStream(fis, null, o);
        fis.close();

        int scale = 1;
        if (o.outHeight > IMAGE_MAX_SIZE || o.outWidth > IMAGE_MAX_SIZE) {
            scale = (int) Math.pow(2, (int) Math.round(Math.log(IMAGE_MAX_SIZE / (double) Math.max(o.outHeight, o.outWidth)) / Math.log(0.5)));
        }

        //Decode with inSampleSize
        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        fis = new FileInputStream(f);
        b = BitmapFactory.decodeStream(fis, null, o2);
        fis.close();
        return b;
    }
}
