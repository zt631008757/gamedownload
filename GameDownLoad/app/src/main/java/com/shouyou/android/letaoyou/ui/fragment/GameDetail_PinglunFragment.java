package com.shouyou.android.letaoyou.ui.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.shouyou.android.letaoyou.R;
import com.shouyou.android.letaoyou.adapter.PingLunAdapter;
import com.shouyou.android.letaoyou.adapter.TuiJian_GameListAdapter;
import com.shouyou.android.letaoyou.bean.GameInfo;
import com.shouyou.android.letaoyou.bean.ScoreInfo;
import com.shouyou.android.letaoyou.dialog.CommInput_Dialog;
import com.shouyou.android.letaoyou.interface_.CommCallBack;
import com.shouyou.android.letaoyou.interface_.OkHttpCallBack;
import com.shouyou.android.letaoyou.manager.API_LoginManager;
import com.shouyou.android.letaoyou.manager.UserManager;
import com.shouyou.android.letaoyou.responce.GetPingLunReponce;
import com.shouyou.android.letaoyou.tool.CommToast;
import com.shouyou.android.letaoyou.ui.activity.PingLunActivity;
import com.shouyou.android.letaoyou.ui.view.CompletedView;
import com.shouyou.android.letaoyou.ui.view.CustomCircleProgressBar;

import java.util.List;

/**
 * Created by Administrator on 2018/8/20.
 */

public class GameDetail_PinglunFragment extends BaseFragment {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mContext = getActivity();
        rootView = inflater.inflate(R.layout.fragment_gamedetail_pinglun, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
//        getData();
//        score();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (progressbar != null) {
            getData();
            score();
        }
    }

    Context mContext;
    GameInfo gameInfo;

    View rootView;
    TextView tv_text, tv_pingjia, tv_score,tv_empty;
    RecyclerView recyclerview;
    CustomCircleProgressBar progressbar;
    RatingBar ratingbar;
    LinearLayout ll_container;

    PingLunAdapter adapter;

    private void initView() {
        tv_text = rootView.findViewById(R.id.tv_text);
        tv_pingjia = rootView.findViewById(R.id.tv_pingjia);
        recyclerview = rootView.findViewById(R.id.recyclerview);

        recyclerview = rootView.findViewById(R.id.recyclerview);
        recyclerview.setLayoutManager(new LinearLayoutManager(mContext));
        adapter = new PingLunAdapter(mContext, commCallBack);
        recyclerview.setAdapter(adapter);
        recyclerview.setHasFixedSize(true);
        recyclerview.setNestedScrollingEnabled(false);

        progressbar = rootView.findViewById(R.id.progressbar);
        ratingbar = rootView.findViewById(R.id.ratingbar);
        tv_score = rootView.findViewById(R.id.tv_score);
        ll_container = rootView.findViewById(R.id.ll_container);
        tv_empty= rootView.findViewById(R.id.tv_empty);

        tv_pingjia.setOnClickListener(this);
    }

    public void setData(GameInfo gameInfo) {
        this.gameInfo = gameInfo;
    }

    CommCallBack commCallBack = new CommCallBack() {
        @Override
        public void onResult(Object obj) {
            getData();
        }
    };

    private void getData() {
        API_LoginManager.comments_list(mContext, gameInfo.id, "", new OkHttpCallBack() {
            @Override
            public void onSuccess(String result) {
                try {
                    GetPingLunReponce reponce = new Gson().fromJson(result, GetPingLunReponce.class);
                    adapter.setData(reponce.data);
                    if(reponce.data==null || reponce.data.size()==0)
                    {
                        tv_empty.setVisibility(View.VISIBLE);
                    }
                    else
                    {
                        tv_empty.setVisibility(View.GONE);
                    }

                } catch (Exception e) {

                }
            }

            @Override
            public void onFailure(String result) {

            }
        });
    }

    private void score() {
        API_LoginManager.score(mContext, gameInfo.id, new OkHttpCallBack() {
            @Override
            public void onSuccess(String result) {
                try {
                    List<ScoreInfo> list = new Gson().fromJson(result, new TypeToken<List<ScoreInfo>>() {
                    }.getType());
                    //计算平均分
                    float peopleCount = 0;
                    float scoreCount = 0;
                    if (list != null && list.size() > 0) {
                        for (int i = 0; i < list.size(); i++) {
                            peopleCount += list.get(i).count;
                            scoreCount += list.get(i).score * list.get(i).count;
                        }
                    }
                    float avg = 0.0f;
                    if(peopleCount != 0)
                    {
                        avg = scoreCount / peopleCount;
                    }
                    progressbar.setProgress(avg);
                    ratingbar.setRating(avg);
                    tv_score.setText(avg + "");

                    showProgress(list);
                } catch (Exception e) {

                }
            }

            @Override
            public void onFailure(String result) {

            }
        });
    }

    private void showProgress(List<ScoreInfo> list) {
        if (list != null && list.size() > 0) {
            //总人数
            int peopleCount = 0;
            if (list != null && list.size() > 0) {
                for (int i = 0; i < list.size(); i++) {
                    peopleCount += list.get(i).count;
                }
            }

            ll_container.removeAllViews();
            for (int i = list.size() - 1; i >= 0; i--) {
                View view = View.inflate(mContext, R.layout.view_progress, null);
                TextView tv_text = view.findViewById(R.id.tv_text);
                ProgressBar progressbar = view.findViewById(R.id.progressbar);
                tv_text.setText(list.get(i).score + "星");

                progressbar.setMax(peopleCount * 100);
                progressbar.setProgress(list.get(i).count * 100);

                ll_container.addView(view);
            }
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.tv_pingjia:
                if (!UserManager.isLogin(mContext)) {
                    UserManager.showLogin(mContext);
                    CommToast.showToast(mContext, "请先登录");
                    return;
                }

                Intent intent = new Intent(mContext, PingLunActivity.class);
                intent.putExtra("gameId", gameInfo.id);
                startActivity(intent);
                break;
        }
    }


}
