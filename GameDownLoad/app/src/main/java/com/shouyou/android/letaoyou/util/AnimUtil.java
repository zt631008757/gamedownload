package com.shouyou.android.letaoyou.util;

import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;

import com.shouyou.android.letaoyou.interface_.CommCallBack;


/**
 * Created by zt on 2018/6/30.
 * 通用动画
 */

public class AnimUtil {

    static int animTime = 300;

    //从顶部 进入界面
    public static void enterFromTop(View view)
    {
        TranslateAnimation ta = new TranslateAnimation(Animation.RELATIVE_TO_PARENT, 0, Animation.RELATIVE_TO_PARENT, 0, Animation.RELATIVE_TO_SELF, -1, Animation.RELATIVE_TO_SELF, 0);
        ta.setDuration(animTime);
        view.startAnimation(ta);
    }

    //返回顶部 退出界面
    public static void outToTop(View view,final CommCallBack callBack)
    {
        TranslateAnimation ta = new TranslateAnimation(Animation.RELATIVE_TO_PARENT, 0, Animation.RELATIVE_TO_PARENT, 0, Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, -1);
        ta.setDuration(animTime);
        ta.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if(callBack!=null)
                    callBack.onResult(null);
            }
        });
        view.startAnimation(ta);
    }


    //从底部 进入界面
    public static void enterFromBottom(View view)
    {
        TranslateAnimation ta = new TranslateAnimation(Animation.RELATIVE_TO_PARENT, 0, Animation.RELATIVE_TO_PARENT, 0, Animation.RELATIVE_TO_SELF, 1, Animation.RELATIVE_TO_SELF, 0);
        ta.setDuration(animTime);
        view.startAnimation(ta);
    }

    //返回底部 退出界面
    public static void outToBottom(View view, final CommCallBack callBack)
    {
        TranslateAnimation ta = new TranslateAnimation(Animation.RELATIVE_TO_PARENT, 0, Animation.RELATIVE_TO_PARENT, 0, Animation.RELATIVE_TO_SELF, 0, Animation.RELATIVE_TO_SELF, 1);
        ta.setDuration(animTime);
        ta.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if(callBack!=null)
                    callBack.onResult(null);
            }
        });
        view.startAnimation(ta);
    }

    //渐变进入
    public static void fadeIn(View view)
    {
        //背景显示
        AlphaAnimation aa = new AlphaAnimation(0, 1);
        aa.setDuration(animTime);
        view.startAnimation(aa);
    }

    //渐变退出
    public static void fadeOut(View view, final CommCallBack callBack)
    {
        //背景显示
        AlphaAnimation aa = new AlphaAnimation(1, 0);
        aa.setDuration(animTime);
        aa.setFillAfter(true);
        aa.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if(callBack!=null)
                callBack.onResult(null);
            }
        });
        view.startAnimation(aa);
    }
}
