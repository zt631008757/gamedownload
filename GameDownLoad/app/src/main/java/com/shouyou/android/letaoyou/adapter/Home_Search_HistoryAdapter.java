package com.shouyou.android.letaoyou.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.shouyou.android.letaoyou.R;
import com.shouyou.android.letaoyou.constant.SPConstants;
import com.shouyou.android.letaoyou.interface_.CommCallBack;
import com.shouyou.android.letaoyou.tool.SPUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2018/8/22.
 */

public class Home_Search_HistoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context mContext;
    List<String> list = new ArrayList<>();
    CommCallBack callBack;

    public Home_Search_HistoryAdapter(Context mContext, CommCallBack callBack) {
        this.mContext = mContext;
        this.callBack = callBack;
    }

    public void setData(List<String> list) {
        this.list = list;
        notifyDataSetChanged();
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        ViewGroup viewGroup = (ViewGroup) mInflater.inflate(R.layout.item_home_search_history, null, false);
        ContentViewHolder holder = new ContentViewHolder(viewGroup);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ContentViewHolder viewHolder = (ContentViewHolder) holder;
        final String info = list.get(position);
        viewHolder.tv_name.setText(info);
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (callBack != null) {
                    callBack.onResult(info);
                }
            }
        });
        viewHolder.tv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearHis(info);
                if (callBack != null) {
                    callBack.onResult(null);
                }
            }
        });
    }

    private void clearHis(String text) {
        String listStr = SPUtil.getStringValue(mContext, SPConstants.HOME_SEARCH_HISTORY, "");
        List<String> searchList = new Gson().fromJson(listStr, new TypeToken<List<String>>() {
        }.getType());
        if (searchList == null) searchList = new ArrayList<>();
        searchList.remove(text);
        SPUtil.putValue(mContext, SPConstants.HOME_SEARCH_HISTORY, new Gson().toJson(searchList));
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }


    class ContentViewHolder extends RecyclerView.ViewHolder {
        TextView tv_name;
        ImageView tv_delete;

        public ContentViewHolder(View itemView) {
            super(itemView);
            tv_name = (TextView) itemView.findViewById(R.id.tv_name);
            tv_delete = (ImageView) itemView.findViewById(R.id.tv_delete);
        }
    }
}
