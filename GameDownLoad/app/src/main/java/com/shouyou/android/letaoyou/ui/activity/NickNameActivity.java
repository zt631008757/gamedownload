package com.shouyou.android.letaoyou.ui.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.shouyou.android.letaoyou.R;
import com.shouyou.android.letaoyou.bean.UserInfo;
import com.shouyou.android.letaoyou.interface_.OkHttpCallBack;
import com.shouyou.android.letaoyou.manager.API_LoginManager;
import com.shouyou.android.letaoyou.manager.UserManager;
import com.shouyou.android.letaoyou.tool.CommToast;
import com.shouyou.android.letaoyou.ui.view.MultiStateView;
import com.shouyou.android.letaoyou.util.Util;

/**
 * Created by Administrator on 2018/6/20.
 */

public class NickNameActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nickname);
        initView();
    }

    EditText et_nickname;
    TextView tv_save;

    MultiStateView multiplestatusView;

    private void initView() {
        setTitle("修改昵称");
        setLeftImgClickListener();

        et_nickname = findViewById(R.id.et_nickname);
        tv_save = findViewById(R.id.tv_save);

        tv_save.setOnClickListener(this);

        UserInfo userInfo = UserManager.getUserInfo(mContext);
        et_nickname.setText(userInfo.nickname);
        if (!TextUtils.isEmpty(userInfo.nickname)) {
            et_nickname.setSelection(userInfo.nickname.length());
        }

    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.tv_save:
                save();
                break;
        }
    }

    private void save() {
        String nickname = et_nickname.getText().toString().trim();
        if (TextUtils.isEmpty(nickname)) {
            CommToast.showToast(mContext, "请输入昵称");
            return;
        }
        showLoadingDialog("");

        API_LoginManager.setting(mContext, nickname, null, new OkHttpCallBack() {
            @Override
            public void onSuccess(String result) {
                dismissLoadingDialog();
                try {
                    if ("\"success\"".equals(result)) {
                        CommToast.showToast(mContext, "修改成功");
                        finish();
                    } else {
                        CommToast.showToast(mContext, Util.unicodeToCn(result));
                    }
                } catch (Exception e) {

                }
            }

            @Override
            public void onFailure(String result) {
                dismissLoadingDialog();
            }
        });
    }
}
