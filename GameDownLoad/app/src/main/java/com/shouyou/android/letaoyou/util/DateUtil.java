package com.shouyou.android.letaoyou.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Administrator on 2018/8/15.
 * 时间工具类
 */

public class DateUtil {
    /**
     * 时间戳转日期
     *
     * @param timeLong 给定的时间戳
     * @return 当前日期
     */
    public static String getDateFromTimeLine(long timeLong) {
        if (timeLong == 0) {
            return "";
        }
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return format.format(new Date(timeLong));
    }
}
