package com.shouyou.android.letaoyou.ui.view;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.shouyou.android.letaoyou.R;
import com.shouyou.android.letaoyou.util.Util;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Administrator on 2016/7/9.
 */
public class MyScrollTabIndicator extends RelativeLayout {

	public String normalTextColor="#333333";   //文字默认颜色
	public String selectTextColor="#fe2641"; //文字选中颜色
	public int bottomViewWidth = 20;   //底部滑块宽度

	Context context;
	List<RelativeLayout> childViews = new ArrayList();
	RelativeLayout preSelectView =null;
	OnTabChangedListener onTabChangedListener;
	int currentSelectIndex = 0 ;
	LinearLayout linearLayout ;
	public MyScrollTabIndicator(Context context) {
		super(context);
		this.context=context;
		initView();
	}

	public MyScrollTabIndicator(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context=context;
		initView();
	}

	public MyScrollTabIndicator(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		this.context=context;
		initView();
	}

	View rootView ;
	LinearLayout ll_container ;
	View view_selected_new;
	HorizontalScrollView scrollView;
	private void initView()
	{
		rootView = View.inflate(context, R.layout.widget_myscrolltabindicator,this);
		ll_container = (LinearLayout) rootView.findViewById(R.id.ll_container);
		view_selected_new = rootView.findViewById(R.id.view_selected_new);
		scrollView = (HorizontalScrollView) rootView.findViewById(R.id.scrollview);

//		linearLayout =new LinearLayout(context);
//		FrameLayout.LayoutParams params =new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
//		linearLayout.setOrientation(LinearLayout.HORIZONTAL);
//		linearLayout.setLayoutParams(params);
	}


	public interface OnTabChangedListener
	{
		void onTabChanged(int index, boolean isClick);
	}

	//绑定数据
	public void setData(List<String> strings, boolean isFullScreen)
	{
		currentSelectIndex = 0;
		childViews.clear();
		ll_container.removeAllViews();
		for (int i=0; i < strings.size(); i++)
		{
			RelativeLayout relativeLayout = (RelativeLayout) View.inflate(context, R.layout.widget_myscrolltab, null);
			TextView title = (TextView) relativeLayout.findViewById(R.id.title);
			title.setText(strings.get(i));
			title.setTextColor(Color.parseColor(normalTextColor));
			if(isFullScreen)
			{
				LinearLayout.LayoutParams params=new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT);
				params.weight=1;
				relativeLayout.setLayoutParams(params);
			}
			else {
				if (i == strings.size() - 1) {
					LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
					params.setMargins(0, 0, Util.dip2px(context, 13), 0);
					relativeLayout.setLayoutParams(params);
				}
			}


			final int finalI = i;
			relativeLayout.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					currentSelectIndex = finalI;
					if(onTabChangedListener!=null)
					{
						onTabChangedListener.onTabChanged(finalI, true);
					}
					setSelectedPosition(finalI);
					System.gc();
				}
			});
			childViews.add(relativeLayout);
			ll_container.addView(relativeLayout);
		}
		view_selected_new.setVisibility(VISIBLE);
	}

	//绑定点击事件
	public void setOnTabChangedListener(OnTabChangedListener onTabChangedListener)
	{
		this.onTabChangedListener = onTabChangedListener;
	}

	public void setSelect(int index)
	{
		currentSelectIndex = index;
		if(onTabChangedListener!=null)
		{
			onTabChangedListener.onTabChanged(index, false);
		}
		setSelectedPosition(index);
		System.gc();
	}

	//切换
	public void setSelectedPosition(int index)
	{
		if(preSelectView!=null)
		{
			TextView title = (TextView) preSelectView.findViewById(R.id.title);
			title.setTextColor(Color.parseColor(normalTextColor));
//			preSelectView.findViewById(R.id.line).setVisibility(GONE);
		}
		RelativeLayout relativeLayout = childViews.get(index);
		TextView title = (TextView) relativeLayout.findViewById(R.id.title);
		title.setTextColor(getResources().getColor(R.color.mainColor));
//		relativeLayout.findViewById(R.id.line).setVisibility(VISIBLE);
		
		//滚动
		int scrollX = relativeLayout.getLeft() + relativeLayout.getWidth()/2 - context.getResources().getDisplayMetrics().widthPixels/2;
		if(scrollView!=null)
		scrollView.smoothScrollTo(scrollX, 0);
		preSelectView =relativeLayout;
	}

	public void refresh()
	{
		if(onTabChangedListener!=null)
		{
			onTabChangedListener.onTabChanged(currentSelectIndex, false);
		}
	}

	/**
	 * viewpager滚动时 调用此方法
	 * @param i
	 * @param v
	 */
	public void setOnscroll(int i, float v){
		if(childViews.size()==0)  return;
		RelativeLayout relativeLayout = childViews.get(i);
		TextView title = (TextView) relativeLayout.findViewById(R.id.title);
		float del = 0;
		float left = 0;
		float width = 0;
		if (i == childViews.size() - 1) {
			left = relativeLayout.getLeft() + title.getLeft() + (title.getWidth() - Util.dip2px(getContext(), bottomViewWidth)) / 2;
			width = title.getWidth();
		} else {
			del = (relativeLayout.getWidth() + (childViews.get(i + 1).findViewById(R.id.title).getLeft() +
					(childViews.get(i + 1).findViewById(R.id.title).getWidth() - Util.dip2px(getContext(), bottomViewWidth)) / 2
					- (title.getWidth() - Util.dip2px(getContext(), bottomViewWidth)) / 2
					- title.getLeft())) * v;
			left = relativeLayout.getLeft() + title.getLeft() + del
					+ (title.getWidth() - Util.dip2px(getContext(), bottomViewWidth)) / 2
			;
			width = title.getWidth() + (childViews.get(i + 1).findViewById(R.id.title).getWidth() - title.getWidth()) * v;
		}
		LayoutParams params = new LayoutParams(Util.dip2px(getContext(), bottomViewWidth), Util.dip2px(getContext(), 3));
		params.setMargins((int) left, 0, 0, 0);
		view_selected_new.setLayoutParams(params);
	}

}
