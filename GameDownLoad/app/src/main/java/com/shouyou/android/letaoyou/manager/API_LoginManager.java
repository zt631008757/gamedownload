package com.shouyou.android.letaoyou.manager;

import android.content.Context;

import com.lzy.okgo.model.HttpMethod;
import com.shouyou.android.letaoyou.bean.TagInfo;
import com.shouyou.android.letaoyou.bean.UserInfo;
import com.shouyou.android.letaoyou.constant.ApiConstants;
import com.shouyou.android.letaoyou.interface_.OkHttpCallBack;
import com.shouyou.android.letaoyou.responce.BaseResponce;
import com.shouyou.android.letaoyou.responce.RegistResponce;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2018/8/29.
 */

public class API_LoginManager {

    //注册
    public static void register(Context context, String mobile, String password, String password_confirmation, OkHttpCallBack callBack) {
        String url = Config.SERVER_HOST + ApiConstants.register;
        Map<String, String> bodyParames = new HashMap<>();
        bodyParames.put("mobile", mobile);
        bodyParames.put("password", password);
        bodyParames.put("password_confirmation", password_confirmation);
        OkHttpManager.okHttpRequest(context, HttpMethod.POST, url, bodyParames, callBack);
    }

    //登录
    public static void login(Context context, String mobile, String password, OkHttpCallBack callBack) {
        String url = Config.SERVER_HOST + ApiConstants.login;
        Map<String, String> bodyParames = new HashMap<>();
        bodyParames.put("mobile", mobile);
        bodyParames.put("password", password);
        OkHttpManager.okHttpRequest(context, HttpMethod.POST, url, bodyParames, callBack);
    }

    //第三方登录
    public static void third_login(Context context, String type, String openid, String nickname, String avatar, OkHttpCallBack callBack) {
        String url = Config.SERVER_HOST + ApiConstants.third_login;
        Map<String, String> bodyParames = new HashMap<>();
        bodyParames.put("type", type);
        bodyParames.put("openid", openid);
        bodyParames.put("nickname", nickname);
        bodyParames.put("avatar", avatar);
        OkHttpManager.okHttpRequest(context, HttpMethod.POST, url, bodyParames, callBack);
    }


    //获取用户信息
    public static void userinfo(Context context, OkHttpCallBack callBack) {
        String url = Config.SERVER_HOST + ApiConstants.userinfo;
        OkHttpManager.okHttpRequest(context, HttpMethod.GET, url, null, callBack);
    }

    //获取游戏类型带游戏
    public static void games_type_list(Context context, OkHttpCallBack callBack) {
        String url = Config.SERVER_HOST + ApiConstants.games_type_list;
        OkHttpManager.okHttpRequest(context, HttpMethod.GET, url, null, callBack);
    }

    //游戏详情
    public static void games_detail(Context context, String id, OkHttpCallBack callBack) {
        String url = Config.SERVER_HOST + ApiConstants.games_detail;
        Map<String, String> bodyParames = new HashMap<>();
        bodyParames.put("id", id);
        OkHttpManager.okHttpRequest(context, HttpMethod.GET, url, bodyParames, callBack);
    }

    //文章列表    1 => ‘首页’,2 => ‘推荐’, 3 => ‘看点’, 4 => ‘活动’,5 => ‘咨询’,
    public static void articles(Context context, String title, String type, OkHttpCallBack callBack) {
        String url = Config.SERVER_HOST + ApiConstants.articles;
        Map<String, String> bodyParames = new HashMap<>();
        bodyParames.put("title", title);
        bodyParames.put("type", type);
        OkHttpManager.okHttpRequest(context, HttpMethod.GET, url, bodyParames, callBack);
    }

    //游戏详情
    public static void comments_list(Context context, String application_id, String parent_id, OkHttpCallBack callBack) {
        String url = Config.SERVER_HOST + ApiConstants.comments_list;
        Map<String, String> bodyParames = new HashMap<>();
        bodyParames.put("application_id", application_id);
        bodyParames.put("parent_id", parent_id);
        OkHttpManager.okHttpRequest(context, HttpMethod.GET, url, bodyParames, callBack);
    }

    //游戏评论
    public static void save_comments(Context context, String id, String parent_id, String score, String content, OkHttpCallBack callBack) {
        String url = Config.SERVER_HOST + ApiConstants.save_comments;
        Map<String, String> bodyParames = new HashMap<>();
        bodyParames.put("id", id);
        bodyParames.put("parent_id", parent_id);
        bodyParames.put("score", score);
        bodyParames.put("content", content);
        OkHttpManager.okHttpRequest(context, HttpMethod.POST, url, bodyParames, callBack);
    }

    //礼包列表
    public static void gifts_list(Context context, String id, OkHttpCallBack callBack) {
        String url = Config.SERVER_HOST + ApiConstants.gifts_list;
        Map<String, String> bodyParames = new HashMap<>();
        bodyParames.put("id", id);
        OkHttpManager.okHttpRequest(context, HttpMethod.GET, url, bodyParames, callBack);
    }

    //礼包列表
    public static void get_code(Context context, String id, OkHttpCallBack callBack) {
        String url = Config.SERVER_HOST + ApiConstants.get_code;
        Map<String, String> bodyParames = new HashMap<>();
        bodyParames.put("id", id);
        OkHttpManager.okHttpRequest(context, HttpMethod.POST, url, bodyParames, callBack);
    }

    //文章详情
    public static void article(Context context, String id, OkHttpCallBack callBack) {
        String url = Config.SERVER_HOST + ApiConstants.article;
        Map<String, String> bodyParames = new HashMap<>();
        bodyParames.put("id", id);
        OkHttpManager.okHttpRequest(context, HttpMethod.GET, url, bodyParames, callBack);
    }

    //游戏列表    type [0 => ‘普通’,1 => ‘H5’,2 => ‘BT’] ， is_new 是否新游   1是0否   ，  is_recommend  是否推荐 1是0否
    public static void games(Context context, String tag, String name, String type, String is_new, String is_recommend, OkHttpCallBack callBack) {
        String url = Config.SERVER_HOST + ApiConstants.games;
        Map<String, String> bodyParames = new HashMap<>();
        bodyParames.put("tag", tag);
        bodyParames.put("name", name);
        bodyParames.put("type", type);
        bodyParames.put("is_new", is_new);
        bodyParames.put("is_recommend", is_recommend);
        OkHttpManager.okHttpRequest(context, HttpMethod.GET, url, bodyParames, callBack);
    }

    //意见反馈
    public static void feedback(Context context, String content, String qq, String mobile, OkHttpCallBack callBack) {
        String url = Config.SERVER_HOST + ApiConstants.feedback;
        Map<String, String> bodyParames = new HashMap<>();
        bodyParames.put("content", content);
        bodyParames.put("qq", qq);
        bodyParames.put("mobile", mobile);
        OkHttpManager.okHttpRequest(context, HttpMethod.POST, url, bodyParames, callBack);
    }

    //链接
    public static void link(Context context, OkHttpCallBack callBack) {
        String url = Config.SERVER_HOST + ApiConstants.link;
        OkHttpManager.okHttpRequest(context, HttpMethod.GET, url, null, callBack);
    }

    //游戏评分
    public static void score(Context context,String application_id, OkHttpCallBack callBack) {
        String url = Config.SERVER_HOST + ApiConstants.score;
        Map<String, String> bodyParames = new HashMap<>();
        bodyParames.put("application_id", application_id);
        OkHttpManager.okHttpRequest(context, HttpMethod.GET, url, bodyParames, callBack);
    }

    //点赞
    public static void like(Context context,String id, OkHttpCallBack callBack) {
        String url = Config.SERVER_HOST + ApiConstants.like;
        Map<String, String> bodyParames = new HashMap<>();
        bodyParames.put("id", id);
        OkHttpManager.okHttpRequest(context, HttpMethod.POST, url, bodyParames, callBack);
    }
    //设置昵称 头像
    public static void setting(Context context, String nickname, File file,OkHttpCallBack callBack) {
        String url = Config.SERVER_HOST + ApiConstants.setting;
        Map<String, String> bodyParames = new HashMap<>();
        bodyParames.put("nickname", nickname);
        OkHttpManager.okHttpFileUpload(context, url, bodyParames,file, callBack);
    }

}
