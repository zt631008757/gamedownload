package com.shouyou.android.letaoyou.ui.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.model.Progress;
import com.lzy.okgo.request.GetRequest;
import com.lzy.okserver.OkDownload;
import com.lzy.okserver.download.DownloadListener;
import com.lzy.okserver.download.DownloadTask;
import com.shouyou.android.letaoyou.R;
import com.shouyou.android.letaoyou.adapter.FragmentViewPagerAdapter;
import com.shouyou.android.letaoyou.bean.GameInfo;
import com.shouyou.android.letaoyou.interface_.OkHttpCallBack;
import com.shouyou.android.letaoyou.manager.API_LoginManager;
import com.shouyou.android.letaoyou.tool.Log;
import com.shouyou.android.letaoyou.ui.fragment.GameDetail_FuliFragment;
import com.shouyou.android.letaoyou.ui.fragment.GameDetail_JianjieFragment;
import com.shouyou.android.letaoyou.ui.fragment.GameDetail_PinglunFragment;
import com.shouyou.android.letaoyou.ui.view.MultiStateView;
import com.shouyou.android.letaoyou.ui.view.MyTabIndicator;
import com.shouyou.android.letaoyou.util.GlideUtil;
import com.shouyou.android.letaoyou.util.StatusBarUtil;
import com.shouyou.android.letaoyou.util.Util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2018/6/20.
 */

public class GameDetailActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //设置沉浸式
        StatusBarUtil.setTranslucentStatus(this);
        mContext = this;
        setStatuBarPadding(false);
        gameId = getIntent().getStringExtra("gameId");
        gameName = getIntent().getStringExtra("gameName");
        setContentView(R.layout.activity_gamedetail1);
        initView();
        getData();
        initDownLoad();
    }

    String gameId;
    String gameName;

    GameInfo gameInfo;
    Context mContext;

    MultiStateView multiplestatusView;
    ImageView iv_head, tv_back;
    TextView tv_name, tv_remarke, tv_type, tv_size;
    MyTabIndicator mytabindicator;
    ViewPager viewpager;
    LinearLayout ll_title;
    View view_space1;

    TextView tv_download, tv_progress, tv_finish;
    ProgressBar progressbar;
    RelativeLayout rl_loading, rl_head;
    View view_space;
    Toolbar toolbar;
    AppBarLayout app_bar;

    GameDetail_JianjieFragment jianjieFragment;
    GameDetail_PinglunFragment pinglunFragment;
    GameDetail_FuliFragment fuliFragment;

    private void initView() {
        setLeftImgClickListener();
        setTitle(gameName);
        view_space = findViewById(R.id.view_space);
        view_space.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, Util.getStatusBarHeight(mContext)));

        multiplestatusView = findViewById(R.id.multiplestatusView);
        multiplestatusView.setViewState(MultiStateView.ViewState.LOADING);
        multiplestatusView.setOnRetryListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                multiplestatusView.setViewState(MultiStateView.ViewState.LOADING);
                getData();
            }
        });
        mytabindicator = findViewById(R.id.mytabindicator);
        viewpager = findViewById(R.id.viewpager);

        //业务部分
        List<String> strs = new ArrayList<>();
        strs.add("简介");
        strs.add("评论");
        strs.add("福利");
        mytabindicator.setData(strs, true);
        List<Fragment> fragmentList = new ArrayList<Fragment>();
        jianjieFragment = new GameDetail_JianjieFragment();
        pinglunFragment = new GameDetail_PinglunFragment();
        fuliFragment = new GameDetail_FuliFragment();


        fragmentList.add(jianjieFragment);
        fragmentList.add(pinglunFragment);
        fragmentList.add(fuliFragment);
        FragmentViewPagerAdapter viewPagerAdapter = new FragmentViewPagerAdapter(getSupportFragmentManager(), fragmentList == null ? null : fragmentList);
        viewpager.setAdapter(viewPagerAdapter);
        viewpager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                //当viewPager发生改变时，同时改变tabhost上面的currentTab
                mytabindicator.setSelectedPosition(position);
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
                mytabindicator.setOnscroll(arg0, arg1);
            }

            @Override
            public void onPageScrollStateChanged(int arg0) {
            }
        });
        //点击tabhost中的tab时，要切换下面的viewPager
        mytabindicator.setOnTabChangedListener(new MyTabIndicator.OnTabChangedListener() {
            @Override
            public void onTabChanged(int index) {
                viewpager.setCurrentItem(index);
            }
        });
        mytabindicator.setSelectedPosition(0);


        iv_head = findViewById(R.id.iv_head);
        tv_name = findViewById(R.id.tv_name);
        tv_remarke = findViewById(R.id.tv_remarke);
        tv_type = findViewById(R.id.tv_type);
        tv_size = findViewById(R.id.tv_size);
        toolbar = findViewById(R.id.toolbar);
        tv_back = findViewById(R.id.tv_back);
        app_bar = findViewById(R.id.app_bar);
        rl_head = findViewById(R.id.rl_head);
        ll_title = findViewById(R.id.ll_title);
        view_space1= findViewById(R.id.view_space1);

        tv_download = findViewById(R.id.tv_download);
        progressbar = findViewById(R.id.progressbar);
        tv_progress = findViewById(R.id.tv_progress);
        rl_loading = findViewById(R.id.rl_loading);
        tv_finish = findViewById(R.id.tv_finish);

        toolbar.setLayoutParams(new CollapsingToolbarLayout.LayoutParams(CollapsingToolbarLayout.LayoutParams.MATCH_PARENT, Util.dip2px(mContext, 44) + Util.dip2px(mContext, 50) + Util.getStatusBarHeight(mContext)));

//        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(CollapsingToolbarLayout.LayoutParams.MATCH_PARENT, Util.dip2px(mContext, 44) + Util.getStatusBarHeight(mContext));
//        rl_title.setLayoutParams(params);
//        rl_title.setPadding(0, Util.getStatusBarHeight(mContext), 0, 0);
        view_space1.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, Util.getStatusBarHeight(mContext)));

        tv_back.setOnClickListener(this);

        app_bar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                try {
                    int heiht = rl_head.getHeight() + Util.dip2px(mContext, 58) - (Util.dip2px(mContext, 44) + Util.dip2px(mContext, 50) + Util.getStatusBarHeight(mContext));
                    if (-verticalOffset >= heiht - 1) {
                        ll_title.setVisibility(View.VISIBLE);
                    } else {
                        ll_title.setVisibility(View.GONE);
                    }
                } catch (Exception e) {

                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.tv_back:
                finish();
                break;
        }
    }

    private void getData() {
        API_LoginManager.games_detail(mContext, gameId, new OkHttpCallBack() {
            @Override
            public void onSuccess(String result) {
                multiplestatusView.setViewState(MultiStateView.ViewState.CONTENT);
                gameInfo = new Gson().fromJson(result, GameInfo.class);
                bindUI();

                jianjieFragment.setData(gameInfo);
                pinglunFragment.setData(gameInfo);
                fuliFragment.setData(gameInfo);

            }

            @Override
            public void onFailure(String result) {
                multiplestatusView.setViewState(MultiStateView.ViewState.ERROR);
            }
        });
    }

    private void bindUI() {
        if (gameInfo == null) return;

        setTitle(gameInfo.name);

        tv_name.setText(gameInfo.name);
        GlideUtil.displayImage(mContext, gameInfo.logo, iv_head, R.drawable.ico_default);
        tv_remarke.setText(gameInfo.subtitle);

        tv_size.setText(gameInfo.size + "M");
        if (gameInfo.tags != null && gameInfo.tags.size() > 0) {
            tv_type.setText(gameInfo.tags.get(0).name);
        }
    }

    private void initDownLoad() {
        final String tagStr = gameId + "_" + gameName;
        if (OkDownload.getInstance().hasTask(tagStr)) {
            DownloadTask task = OkDownload.getInstance().getTask(tagStr);
            task.register(listener);
            setTask(task);
            setTag(tagStr);
            Progress progress = task.progress;
            refresh(progress);
        } else {
            setTask(null);
            setTag(null);
            rl_loading.setVisibility(View.GONE);  //下载中
            tv_download.setVisibility(View.VISIBLE);  //下载
            tv_finish.setVisibility(View.GONE);  //已完成
        }

        tv_download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (gameInfo == null) return;
                DownloadTask task;
                if (!OkDownload.getInstance().hasTask(tagStr)) {
                    GetRequest<File> request = OkGo.get(gameInfo.download_link);
                    task = OkDownload.request(tagStr, request).save().register(listener);
                    task.extra1(gameInfo);
                    setTask(task);
                    setTag(tagStr);
                    start();
                } else {
                    start();
                }
            }
        });
    }


    public DownloadTask task;
    public String tag;

    public void setTask(DownloadTask task) {
        this.task = task;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getTag() {
        return tag;
    }

    public void refresh(Progress progress) {
        rl_loading.setVisibility(View.GONE);  //下载中
        tv_download.setVisibility(View.GONE);  //下载
        tv_finish.setVisibility(View.GONE);  //已完成
        switch (progress.status) {
            case Progress.NONE:
                tv_download.setText("下载");
                tv_download.setVisibility(View.VISIBLE);  //下载
                break;
            case Progress.PAUSE:
                tv_download.setText("继续");
                tv_download.setVisibility(View.VISIBLE);  //继续
                break;
            case Progress.ERROR:
                tv_download.setText("重试");
                tv_download.setVisibility(View.VISIBLE);  //重试
                break;
            case Progress.WAITING:
                tv_progress.setText("等待中");
                rl_loading.setVisibility(View.VISIBLE);  //等待中
                break;
            case Progress.FINISH:
                tv_finish.setText("安装");
                tv_finish.setVisibility(View.VISIBLE);   //下载完成
                break;
            case Progress.LOADING:    //下载中
                progressbar.setProgress((int) (progress.fraction * 100));

                Log.i("下载中" + progress.fraction * 100);
                rl_loading.setVisibility(View.VISIBLE);  //下载中
                tv_progress.setText(Util.numDecimalFormat(progress.fraction * 100 + "", 2) + "%");
                break;
        }
    }

    public void start() {
        Log.i("start");
        Progress progress = task.progress;
        switch (progress.status) {
            case Progress.PAUSE:
            case Progress.NONE:
            case Progress.ERROR:
                task.start();
                break;
            case Progress.LOADING:
                task.pause();
                break;
            case Progress.FINISH:
//                    MusicPlayUtil.loadR(progress.filePath);
                break;
        }
        refresh(progress);
    }


    DownloadListener listener = new DownloadListener(tag) {
        @Override
        public void onStart(Progress progress) {
            Log.i("开始下载");
            refresh(progress);
        }

        @Override
        public void onProgress(Progress progress) {
            refresh(progress);
        }

        @Override
        public void onError(Progress progress) {
            refresh(progress);
        }

        @Override
        public void onFinish(File file, Progress progress) {
            Log.i("下载完成");
            refresh(progress);
        }

        @Override
        public void onRemove(Progress progress) {

        }
    };

}
