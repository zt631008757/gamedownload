package com.shouyou.android.letaoyou;

import android.content.Intent;
import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.shouyou.android.letaoyou.bean.UserInfo;
import com.shouyou.android.letaoyou.constant.SPConstants;
import com.shouyou.android.letaoyou.interface_.CommCallBack;
import com.shouyou.android.letaoyou.interface_.OkHttpCallBack;
import com.shouyou.android.letaoyou.manager.API_LoginManager;
import com.shouyou.android.letaoyou.manager.UserManager;
import com.shouyou.android.letaoyou.responce.BaseResponce;
import com.shouyou.android.letaoyou.responce.RegistResponce;
import com.shouyou.android.letaoyou.tool.CommToast;
import com.shouyou.android.letaoyou.tool.SPUtil;
import com.shouyou.android.letaoyou.ui.activity.BaseActivity;
import com.shouyou.android.letaoyou.ui.activity.DownLoadListActivity;
import com.shouyou.android.letaoyou.ui.activity.FankuiActivity;
import com.shouyou.android.letaoyou.ui.activity.LoginActivity;
import com.shouyou.android.letaoyou.ui.activity.SearchActivity;
import com.shouyou.android.letaoyou.ui.activity.SettingActivity;
import com.shouyou.android.letaoyou.ui.activity.ShareActivity;
import com.shouyou.android.letaoyou.ui.activity.UserInfoActivity;
import com.shouyou.android.letaoyou.ui.activity.WebViewActivity;
import com.shouyou.android.letaoyou.ui.fragment.Home_Fragment_FaXian;
import com.shouyou.android.letaoyou.ui.fragment.Home_Fragment_TuiJian;
import com.shouyou.android.letaoyou.ui.fragment.Home_Fragment_FenLei;
import com.shouyou.android.letaoyou.ui.view.MyBottomTabView;
import com.shouyou.android.letaoyou.util.GlideUtil;
import com.shouyou.android.letaoyou.util.Util;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends BaseActivity {

    public final static int HOME_INDEX_0 = 0;
    public final static int HOME_INDEX_1 = 1;
    public final static int HOME_INDEX_2 = 2;
    public final static int HOME_INDEX_3 = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initView();
        chooseFragment(HOME_INDEX_0);

        link();
    }

    protected void onResume() {
        super.onResume();
        if (UserManager.isLogin(mContext)) {
            getUserInfo();
        } else {
            bindData();
        }
    }

    MyBottomTabView mybottomtabview;
    DrawerLayout drawerLayout;
    ViewGroup view_main, nav_view;
    ImageView iv_main_head, iv_download, iv_head;
    LinearLayout ll_setting, ll_fankui, ll_gonghuizhoubian, ll_duihuanzhongxin, ll_yaoqinghaoyou,ll_search;

    TextView tv_version, tv_login, tv_username, tv_qiandao;

    private void initView() {
        mybottomtabview = (MyBottomTabView) findViewById(R.id.mybottomtabview);
        mybottomtabview.setOnTabSelectCallBack(onBottomTabSelect);

        view_main = (ViewGroup) findViewById(R.id.view_main);
        nav_view = (ViewGroup) findViewById(R.id.nav_view);

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerLayout.setScrimColor(Color.parseColor("#55555555"));   //设置抽屉打开 背景半透明颜色
//        drawerLayout.addDrawerListener(new DrawerLayout.SimpleDrawerListener() {
//            @Override
//            public void onDrawerSlide(View drawerView, float slideOffset) {
//                super.onDrawerSlide(drawerView, slideOffset);
//                view_main.setX(slideOffset * drawerView.getWidth());
//            }
//        });

        tv_version = findViewById(R.id.tv_version);
        tv_login = findViewById(R.id.tv_login);
        iv_main_head = findViewById(R.id.iv_main_head);
        tv_username = findViewById(R.id.tv_username);
        ll_setting = findViewById(R.id.ll_setting);
        ll_fankui = findViewById(R.id.ll_fankui);
        iv_download = findViewById(R.id.iv_download);
        ll_gonghuizhoubian = findViewById(R.id.ll_gonghuizhoubian);
        tv_qiandao = findViewById(R.id.tv_qiandao);
        ll_duihuanzhongxin = findViewById(R.id.ll_duihuanzhongxin);
        ll_yaoqinghaoyou = findViewById(R.id.ll_yaoqinghaoyou);
        iv_head = findViewById(R.id.iv_head);
        ll_search = findViewById(R.id.ll_search);

        tv_login.setOnClickListener(this);
        iv_main_head.setOnClickListener(this);
        ll_setting.setOnClickListener(this);
        ll_fankui.setOnClickListener(this);
        nav_view.setOnClickListener(this);
        iv_download.setOnClickListener(this);
        ll_gonghuizhoubian.setOnClickListener(this);
        tv_qiandao.setOnClickListener(this);
        ll_duihuanzhongxin.setOnClickListener(this);
        ll_yaoqinghaoyou.setOnClickListener(this);
        iv_head.setOnClickListener(this);
        ll_search.setOnClickListener(this);

        tv_version.setText("v" + Util.getVersionName(mContext));
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        Intent intent;
        switch (view.getId()) {
            case R.id.tv_login:   //登录
                intent = new Intent(mContext, LoginActivity.class);
                startActivity(intent);
                break;
            case R.id.iv_main_head:
                drawerLayout.openDrawer(GravityCompat.START);
                break;
            case R.id.ll_setting:  //设置
                intent = new Intent(mContext, SettingActivity.class);
                startActivity(intent);
                break;
            case R.id.ll_fankui:  //反馈
                if (!UserManager.isLogin(mContext)) {
                    UserManager.showLogin(mContext);
                    CommToast.showToast(mContext, "请先登录");
                    return;
                }
                intent = new Intent(mContext, FankuiActivity.class);
                startActivity(intent);
                break;
            case R.id.ll_duihuanzhongxin:
                CommToast.showToast(mContext, "暂未开放");
                break;
            case R.id.ll_yaoqinghaoyou:
                intent = new Intent(mContext, ShareActivity.class);
                startActivity(intent);
                break;
            case R.id.nav_view:
                //消耗点击事件
                break;
            case R.id.iv_download:    //下载中心
                intent = new Intent(mContext, DownLoadListActivity.class);
                startActivity(intent);
                break;
            case R.id.ll_gonghuizhoubian:   //公会周边
                String url = SPUtil.getStringValue(mContext, SPConstants.Guild_url, "");
                if (!TextUtils.isEmpty("url")) {
                    intent = new Intent(mContext, WebViewActivity.class);
                    intent.putExtra("url", url);
                    intent.putExtra("title", "公会周边");
                    startActivity(intent);
                } else {
                    CommToast.showToast(mContext, "暂无链接");
                }
                break;
            case R.id.tv_qiandao:   //签到
                CommToast.showToast(mContext, "暂未开放");
                break;
            case R.id.iv_head:
                if (!UserManager.isLogin(mContext)) {
                    UserManager.showLogin(mContext);
                    CommToast.showToast(mContext, "请先登录");
                    return;
                }
                intent = new Intent(mContext, UserInfoActivity.class);
                startActivity(intent);
                break;
            case R.id.ll_search:
                intent = new Intent(mContext, SearchActivity.class);
                startActivity(intent);
                break;
        }
    }

    //获取用户信息
    public void getUserInfo() {
        API_LoginManager.userinfo(mContext, new OkHttpCallBack() {
            @Override
            public void onSuccess(String baseResponce) {
                try {
                    UserInfo userInfo = new Gson().fromJson(baseResponce, UserInfo.class);
                    UserManager.saveUserInfo(mContext, userInfo);

                    bindData();
                } catch (Exception e) {

                }
            }

            @Override
            public void onFailure(String baseResponce) {

            }
        });
    }

    //获取链接
    public void link() {
        API_LoginManager.link(mContext, new OkHttpCallBack() {
            @Override
            public void onSuccess(String baseResponce) {
                try {
                    JSONObject jsonObject = new JSONObject(baseResponce);
                    if (jsonObject.has("live_url")) {
                        SPUtil.putValue(mContext, SPConstants.Live_url, jsonObject.getString("live_url"));
                    }
                    if (jsonObject.has("guild_url")) {
                        SPUtil.putValue(mContext, SPConstants.Guild_url, jsonObject.getString("guild_url"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(String baseResponce) {

            }
        });
    }

    private void bindData() {
        UserInfo userInfo = UserManager.getUserInfo(mContext);
        if (userInfo != null) {
            tv_login.setVisibility(View.GONE);
            tv_username.setText(userInfo.nickname);
            tv_qiandao.setVisibility(View.VISIBLE);
            GlideUtil.displayImage(mContext, userInfo.avatar, iv_head, R.drawable.ico_default_head_fang);
            GlideUtil.displayImage(mContext, userInfo.avatar, iv_main_head, R.drawable.ico_home_head);
        } else {
            tv_login.setVisibility(View.VISIBLE);
            tv_username.setText("请登录");
            tv_qiandao.setVisibility(View.GONE);
            GlideUtil.displayImage(mContext, R.drawable.ico_default_head_fang, iv_head, -1);
            GlideUtil.displayImage(mContext, R.drawable.ico_home_head, iv_main_head, -1);
        }
    }

    private void chooseFragment(int index) {
        Fragment currentFragment = getSupportFragmentManager().findFragmentByTag(String.valueOf(index));
        hideAllFrag();
        if (currentFragment == null) {
            switch (index) {
                case HOME_INDEX_0:     // 推荐
                    currentFragment = Home_Fragment_TuiJian.newInstance();
                    break;
                case HOME_INDEX_1:     //分类
                    currentFragment = Home_Fragment_FenLei.newInstance();
                    break;
                case HOME_INDEX_2:      // 发现
                    currentFragment = Home_Fragment_FaXian.newInstance();
                    break;
            }
            addFragment(currentFragment, index);
        } else {
            showFragment(currentFragment);
        }
        //切换刷新
        currentFragment.onResume();
    }

    CommCallBack onBottomTabSelect = new CommCallBack() {
        @Override
        public void onResult(Object obj) {
            int index = (int) obj;
            chooseFragment(index);
        }
    };

    private void addFragment(Fragment currentFragment, int index) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.add(R.id.framelayout, currentFragment, index + "");
        ft.commitAllowingStateLoss();
    }

    /**
     * 隐藏所有的fragment
     */
    private void hideAllFrag() {
        for (int i = 0; i < 5; i++) {
            Fragment fragment = getSupportFragmentManager().findFragmentByTag(String.valueOf(i));
            if (fragment != null) {
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.hide(fragment);
                ft.commitAllowingStateLoss();
            }
        }
    }

    /**
     * 展示fragment
     */
    public void showFragment(Fragment fragment) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        if (fragment.isDetached()) {
            ft.attach(fragment).show(fragment);
            ft.commitAllowingStateLoss();
        } else {
            ft.show(fragment);
            ft.commitAllowingStateLoss();
        }
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    private long exitTime = 0;

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            if ((System.currentTimeMillis() - exitTime) > 2000) {
                CommToast.showToast(mContext, "再按一次退出程序");
                exitTime = System.currentTimeMillis();
                return true;
            } else {
                return super.onKeyDown(keyCode, event);
            }
        }
        return super.onKeyDown(keyCode, event);
    }


}
