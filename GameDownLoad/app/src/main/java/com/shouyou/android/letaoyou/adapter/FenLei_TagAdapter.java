package com.shouyou.android.letaoyou.adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.shouyou.android.letaoyou.R;
import com.shouyou.android.letaoyou.bean.TagInfo;
import com.shouyou.android.letaoyou.interface_.CommCallBack;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2018/8/22.
 */

public class FenLei_TagAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context mContext;
    List<TagInfo> list = new ArrayList<>();
    CommCallBack callBack;

    public FenLei_TagAdapter(Context mContext, CommCallBack callBack) {
        this.mContext = mContext;
        this.callBack = callBack;
    }

    public void setData(List<TagInfo> list) {
        this.list = list;
        notifyDataSetChanged();
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        ViewGroup viewGroup = (ViewGroup) mInflater.inflate(R.layout.item_taglist, null, false);
        ContentViewHolder holder = new ContentViewHolder(viewGroup);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ContentViewHolder viewHolder = (ContentViewHolder) holder;
        final TagInfo tagInfo = list.get(position);
        viewHolder.tv_tagname.setText(tagInfo.name);


        if (tagInfo.limit_applications != null && tagInfo.limit_applications.size() > 0) {
            FenLei_GameListAdapter adapter = new FenLei_GameListAdapter(mContext, null);
            adapter.setData(tagInfo.limit_applications);
            viewHolder.recyclerview.setAdapter(adapter);

            viewHolder.recyclerview.setVisibility(View.VISIBLE);
        } else {
            viewHolder.recyclerview.setVisibility(View.GONE);
        }

        viewHolder.tv_more.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(callBack!=null)
                {
                    callBack.onResult(tagInfo.id);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
//        return 10;
        return list == null ? 0 : list.size();
    }


    class ContentViewHolder extends RecyclerView.ViewHolder {
        TextView tv_tagname,tv_more;
        RecyclerView recyclerview;

        public ContentViewHolder(View itemView) {
            super(itemView);
            tv_tagname = (TextView) itemView.findViewById(R.id.tv_tagname);
            tv_more = itemView.findViewById(R.id.tv_more);

            recyclerview = itemView.findViewById(R.id.recyclerview);
            LinearLayoutManager lm = new LinearLayoutManager(mContext);
            lm.setOrientation(LinearLayoutManager.HORIZONTAL);
            recyclerview.setLayoutManager(lm);

        }
    }
}
