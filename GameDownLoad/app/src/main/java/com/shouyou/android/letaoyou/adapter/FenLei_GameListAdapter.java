package com.shouyou.android.letaoyou.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.shouyou.android.letaoyou.R;
import com.shouyou.android.letaoyou.bean.GameInfo;
import com.shouyou.android.letaoyou.interface_.CommCallBack;
import com.shouyou.android.letaoyou.ui.activity.GameDetailActivity;
import com.shouyou.android.letaoyou.util.GlideUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2018/8/22.
 */

public class FenLei_GameListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context mContext;
    List<GameInfo> list = new ArrayList<>();
    CommCallBack callBack;

    public FenLei_GameListAdapter(Context mContext, CommCallBack callBack) {
        this.mContext = mContext;
        this.callBack = callBack;
    }

    public void setData(List<GameInfo> list) {
        this.list = list;
        notifyDataSetChanged();
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        ViewGroup viewGroup = (ViewGroup) mInflater.inflate(R.layout.item_tag_gamelist, null, false);
        ContentViewHolder holder = new ContentViewHolder(viewGroup);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ContentViewHolder viewHolder = (ContentViewHolder) holder;
        final GameInfo gameInfo = list.get(position);

        GlideUtil.displayImage(mContext, gameInfo.logo, viewHolder.iv_img, -1);
        viewHolder.tv_name.setText(gameInfo.name);
        if (position == 0) {
            viewHolder.view_left.setVisibility(View.VISIBLE);
        } else {
            viewHolder.view_left.setVisibility(View.GONE);
        }

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, GameDetailActivity.class);
                intent.putExtra("gameId", gameInfo.id);
                intent.putExtra("gameName", gameInfo.name);
                mContext.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }


    class ContentViewHolder extends RecyclerView.ViewHolder {
        TextView tv_text,tv_name;
        ImageView iv_img;
        View view_left, view_right;

        public ContentViewHolder(View itemView) {
            super(itemView);
            tv_text = (TextView) itemView.findViewById(R.id.tv_text);
            iv_img = itemView.findViewById(R.id.iv_img);
            view_left = itemView.findViewById(R.id.view_left);
            view_right = itemView.findViewById(R.id.view_right);
            tv_name= itemView.findViewById(R.id.tv_name);
        }
    }
}
