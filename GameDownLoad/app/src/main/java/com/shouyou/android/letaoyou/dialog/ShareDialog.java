package com.shouyou.android.letaoyou.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.util.Base64;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.shouyou.android.letaoyou.R;
import com.shouyou.android.letaoyou.event.EventBus_ShareSuccess;
import com.shouyou.android.letaoyou.interface_.CommCallBack;
import com.shouyou.android.letaoyou.manager.Config;
import com.shouyou.android.letaoyou.tool.CommToast;
import com.shouyou.android.letaoyou.util.AnimUtil;
import com.shouyou.android.letaoyou.util.StatusBarUtil_Dialog;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;
import java.util.List;

import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.PlatformActionListener;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.onekeyshare.OnekeyShare;
import cn.sharesdk.tencent.qq.QQ;
import cn.sharesdk.wechat.friends.Wechat;

/**
 * Created by Administrator on 2018/6/30.
 */

public class ShareDialog extends Dialog implements View.OnClickListener {

    Context context;

    public ShareDialog(@NonNull Context context) {
        super(context);
        this.context = context;
    }

    public ShareDialog(@NonNull Context context, @StyleRes int themeResId) {
        super(context, themeResId);
        this.context = context;
    }

    protected ShareDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_share);
        initView();
        StatusBarUtil_Dialog.setImmersiveStatusBar(this, true);
    }

    LinearLayout ll_content;
    View view_bg;
    LinearLayout ll_friend, ll_qzone;
    TextView tv_cancel;

    private void initView() {
        ll_content = (LinearLayout) findViewById(R.id.ll_content);
        view_bg = findViewById(R.id.view_bg);
        ll_friend = findViewById(R.id.ll_friend);
        ll_qzone = findViewById(R.id.ll_qzone);
        tv_cancel = findViewById(R.id.tv_cancel);
        AnimUtil.fadeIn(view_bg);
        AnimUtil.fadeIn(ll_content);

        view_bg.setOnClickListener(this);
        ll_content.setOnClickListener(this);
        ll_friend.setOnClickListener(this);
        ll_qzone.setOnClickListener(this);
        tv_cancel.setOnClickListener(this);

        shareUrl = "http://90js.cn/share?key=" + Base64.encodeToString(Config.admin_key.getBytes(), Base64.DEFAULT);
    }

    String title = "乐淘游";
    String content = "分享内容";
    String imgUrl = "https://ss0.baidu.com/6ONWsjip0QIZ8tyhnq/it/u=2386734369,3655898069&fm=179&app=42&f=PNG?w=121&h=140";
    String shareUrl = "";


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.view_bg:
            case R.id.tv_cancel:
                dismissWithAnim();
                break;
            case R.id.ll_content:
                //不处理，只消耗点击事件
                break;
            case R.id.ll_friend:   //QQ
                showShare(QQ.NAME);
                break;
            case R.id.ll_qzone:    //微信
                showShare(Wechat.NAME);
                break;
        }
    }

    private void showShare(String platform) {
        final OnekeyShare oks = new OnekeyShare();
        //指定分享的平台，如果为空，还是会调用九宫格的平台列表界面
        if (platform != null) {
            oks.setPlatform(platform);
        }
        //关闭sso授权
        oks.disableSSOWhenAuthorize();
        // title标题，印象笔记、邮箱、信息、微信、人人网和QQ空间使用
        oks.setTitle(title);
        // titleUrl是标题的网络链接，仅在Linked-in,QQ和QQ空间使用
        oks.setTitleUrl(shareUrl);
        // text是分享文本，所有平台都需要这个字段
        oks.setText(content);
        //分享网络图片，新浪微博分享网络图片需要通过审核后申请高级写入接口，否则请注释掉测试新浪微博
        oks.setImageUrl(imgUrl);
        // imagePath是图片的本地路径，Linked-In以外的平台都支持此参数
        //oks.setImagePath("/sdcard/test.jpg");//确保SDcard下面存在此张图片
        // url仅在微信（包括好友和朋友圈）中使用
        oks.setUrl(shareUrl);
        // comment是我对这条分享的评论，仅在人人网和QQ空间使用
        oks.setComment("我是测试评论文本");
        // site是分享此内容的网站名称，仅在QQ空间使用
        oks.setSite(title);
        // siteUrl是分享此内容的网站地址，仅在QQ空间使用
        oks.setSiteUrl(shareUrl);
        oks.setCallback(new PlatformActionListener() {
            @Override
            public void onComplete(Platform platform, int i, HashMap<String, Object> hashMap) {
                CommToast.showToast(context, "分享成功");
                dismissWithAnim();
                EventBus.getDefault().post(new EventBus_ShareSuccess());
//                ApiManager.shareUnlock(context, groupId, new OkHttpCallBack() {
////                    @Override
////                    public void onSuccess(BaseResponce baseResponce) {
////                        if(baseResponce.result)
////                        {
////                            EventBus.getDefault().post(new EventBus_ShareSuccess());
////                        }
////                    }
////
////                    @Override
////                    public void onFailure(BaseResponce baseResponce) {
////
////                    }
////                });
            }

            @Override
            public void onError(Platform platform, int i, Throwable throwable) {
                CommToast.showToast(context, "分享失败，请重试");
            }

            @Override
            public void onCancel(Platform platform, int i) {
                CommToast.showToast(context, "取消分享");
            }
        });
        //启动分享
        oks.show(context);
    }

    public void dismissWithAnim() {
        AnimUtil.fadeOut(ll_content, null);
        AnimUtil.fadeOut(view_bg, new CommCallBack() {
            @Override
            public void onResult(Object obj) {
                dismiss();
            }
        });
    }

    @Override
    public void onBackPressed() {
        dismissWithAnim();
    }
}
