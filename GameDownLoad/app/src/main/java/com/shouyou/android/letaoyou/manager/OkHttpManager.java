package com.shouyou.android.letaoyou.manager;

import android.content.Context;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.lzy.okgo.OkGo;
import com.lzy.okgo.callback.AbsCallback;
import com.lzy.okgo.callback.StringCallback;
import com.lzy.okgo.model.HttpMethod;
import com.lzy.okgo.model.Response;
import com.lzy.okgo.request.base.BodyRequest;
import com.lzy.okgo.request.base.Request;
import com.shouyou.android.letaoyou.interface_.OkHttpCallBack;
import com.shouyou.android.letaoyou.responce.BaseResponce;
import com.shouyou.android.letaoyou.tool.Log;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * Created by Administrator on 2018/8/13.
 */

public class OkHttpManager {

    /**
     * okHttp请求， 外部调用
     *
     * @param context     上下文，用于取消请求
     * @param methed      请求类型 (HttpMethod.POST/HttpMethod.GET)
     * @param url         请求地址
     * @param requestBody post方式 放入请求体的参数
     * @param callBack    回调对象
     */
    public static void okHttpRequest(Context context, HttpMethod methed, String url, Map<String, String> requestBody, OkHttpCallBack callBack) {
        //headers参数
        Map<String, String> headers = new HashMap<>();
        headers.put("Authorization", "Bearer " + UserManager.getToken(context));
        executeByOkHttp(methed, context, url, requestBody, headers, callBack);
    }

    //文件上传
    public static void okHttpFileUpload(Context context, String url, Map<String, String> requestBody, File file, OkHttpCallBack callBack) {
        //headers参数
        Map<String, String> headers = new HashMap<>();
        headers.put("Authorization", "Bearer " + UserManager.getToken(context));
        uploadByOkhttp(context, url, headers, requestBody, file, callBack);
    }

    private static void uploadByOkhttp(final Context context, final String url, Map<String, String> headers, Map<String, String> bodyParam, File file, final OkHttpCallBack callBack) {
        try {
            // form 表单形式上传
            MultipartBody.Builder requestBody = new MultipartBody.Builder().setType(MultipartBody.FORM);
            if (file != null) {
                // MediaType.parse() 里面是上传的文件类型。
                RequestBody body = RequestBody.create(MediaType.parse("image/*"), file);
                String filename = file.getName();
                // 参数分别为， 请求key ，文件名称 ， RequestBody
                requestBody.addFormDataPart("avatar", file.getName(), body);
//            requestBody.addFormDataPart("image", file.getName(), RequestBody.create(null, file));
            }
            if (bodyParam != null) {
                // map 里面是请求中所需要的 key 和 value
                for (Map.Entry entry : bodyParam.entrySet()) {
                    if (entry.getValue() != null) {
                        requestBody.addFormDataPart(entry.getKey().toString(), entry.getValue().toString());
                    }
                }
            }

            BodyRequest request = OkGo.post(url);
            request.upRequestBody(requestBody.build());
//        request.addFileParams("file", Arrays.asList(file));
//        if (bodyParam != null) {
//            //添加body
//            FormBody.Builder builder = new FormBody.Builder();
//            for (String key : bodyParam.keySet()) {
//                String value = bodyParam.get(key);
//                if (!TextUtils.isEmpty(value)) {
//                    builder.add(key, value);
//                }
//            }
//            FormBody body = builder.build();
//            request.upRequestBody(body);
//        }

            //添加headers
            if (headers != null && headers.size() > 0) {
                for (String key : headers.keySet()) {
                    String value = headers.get(key);
                    request.getHeaders().put(key, value);
                }
            }

            request.execute(new StringCallback() {
                @Override
                public void onSuccess(Response<String> response) {
                    callBack.onSuccess(response.body());
                }

                @Override
                public void onError(Response<String> response) {
                    super.onError(response);
                    callBack.onFailure(response.body());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            callBack.onFailure("");
        }
    }

    private static void executeByOkHttp(HttpMethod methed, final Context context, final String url, Map<String, String> requestBody, Map<String, String> headers, final OkHttpCallBack callBack) {
        try {
            Request request = null;
            if (HttpMethod.POST == methed) {
                //添加body
                FormBody.Builder builder = new FormBody.Builder();
                if (requestBody != null) {
                    for (String key : requestBody.keySet()) {
                        String value = requestBody.get(key);
                        if (!TextUtils.isEmpty(value)) {
                            builder.add(key, value);
                            Log.i(key + ":" + value);
                        }
                    }
                }
                FormBody body = builder.build();
                request = OkGo.post(url).tag(context).upRequestBody(body);
            } else {
                String paramStr = "";
                if (requestBody != null) {
                    paramStr = "?";
                    for (String key : requestBody.keySet()) {
                        String value = requestBody.get(key);
                        if (!TextUtils.isEmpty(value)) {
                            paramStr += key + "=" + value + "&";
                        }
                    }
                }
                request = OkGo.get(url + paramStr).tag(context);
            }

            //添加headers
            if (headers != null && headers.size() > 0) {
                for (String key : headers.keySet()) {
                    String value = headers.get(key);
                    request.getHeaders().put(key, value);
                }
            }
            request.execute(new StringCallback() {
                @Override
                public void onSuccess(Response<String> response) {
                    callBack.onSuccess(response.body());
                }

                @Override
                public void onError(Response<String> response) {
                    super.onError(response);
                    callBack.onFailure(response.body());
                }
            });

//            request.execute(new AbsCallback<Object>() {
//                @Override
//                public Object convertResponse(okhttp3.Response response) throws Throwable {
//                    //子线程
//                    Object object = null;
//                    String s = response.body().string();
//
//                    if (responseClass != null) {
//                        try {
//                            object = new Gson().fromJson(s, responseClass);
//                        } catch (Exception e) {
//                            object = new Gson().fromJson(s, BaseResponce.class);
//                        }
//                    } else {
//                        object = new BaseResponce();
////                        object.code = BaseResponce.Status_Success;
////                        object.result = s;
//                    }
//                    return object;
//                }
//
//                @Override
//                public void onSuccess(Response<Object> response) {
//                    if (callBack == null) return;
//                    Object baseResponce = response.body();
//                    //UI线程
//                    if (baseResponce == null)  //数据解析失败
//                    {
//                        callBack.onFailure(baseResponce);
//                    } else {           //数据解析成功
////                        callBack.onSuccess(baseResponce);
//                        callBack.onSuccess(baseResponce);
//                    }
//                }
//
//                @Override
//                public void onError(Response<Object> response) {
//                    super.onError(response);
//                    if (callBack != null) {
//                        callBack.onFailure(null);
//                    }
//                }
//            });
        } catch (Exception e) {
            e.printStackTrace();
            if (callBack != null) {
                callBack.onFailure(null);
            }
        }
    }

}
