package com.shouyou.android.letaoyou.constant;

/**
 * Created by zt on 2018/8/13.
 * 所有接口地址放这里（相对路径）
 */

public class ApiConstants {
    //登录
    public static final String login = "/api/login";   //登录
    public static final String register = "/api/register";   //注册
    public static final String userinfo = "/api/me";   //获取用户信息
    public static final String games_type_list = "/api/type-with-games";   //游戏类型
    public static final String games_detail = "/api/game";   //游戏详情
    public static final String articles = "/api/articles";   //文章列表
    public static final String comments_list = "/api/comments";   //获取评论
    public static final String save_comments = "/api/comment";   //评论
    public static final String gifts_list = "/api/gifts";   //礼包列表
    public static final String get_code = "/api/get-code";   //领取礼包
    public static final String article = "/api/article";   //文章详情
    public static final String games = "/api/games";   //游戏列表
    public static final String third_login = "/api/oauth";   //游戏列表
    public static final String feedback = "/api/feedback";   //反馈
    public static final String link = "/api/admin/link";   //外链
    public static final String score = "/api/comment-score";   //评分
    public static final String like = "/api/comment/like";   //点赞
    public static final String setting = "/api/user/setting";   //设置
}
