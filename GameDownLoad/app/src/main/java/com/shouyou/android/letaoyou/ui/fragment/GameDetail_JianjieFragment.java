package com.shouyou.android.letaoyou.ui.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.shouyou.android.letaoyou.R;
import com.shouyou.android.letaoyou.adapter.FenLei_TagAdapter;
import com.shouyou.android.letaoyou.adapter.Gamedetail_ImgAdapter;
import com.shouyou.android.letaoyou.bean.GameInfo;
import com.shouyou.android.letaoyou.interface_.CommCallBack;
import com.shouyou.android.letaoyou.ui.activity.BigImage_CommActitivty;

import java.util.ArrayList;

/**
 * Created by Administrator on 2018/8/20.
 */

public class GameDetail_JianjieFragment extends BaseFragment {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mContext = getActivity();
        rootView = inflater.inflate(R.layout.fragment_gamedetail_jianjie, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
        bindUI();
    }

    Context mContext;
    GameInfo gameInfo;


    View rootView;
    TextView tv_text;
    RecyclerView recyclerview;
    Gamedetail_ImgAdapter imgAdapter;

    private void initView() {
        tv_text = rootView.findViewById(R.id.tv_text);

        recyclerview = rootView.findViewById(R.id.recyclerview);
        LinearLayoutManager lm = new LinearLayoutManager(mContext);
        lm.setOrientation(RecyclerView.HORIZONTAL);
        recyclerview.setLayoutManager(lm);
        imgAdapter = new Gamedetail_ImgAdapter(mContext, callBack);
        recyclerview.setAdapter(imgAdapter);

    }

    CommCallBack callBack = new CommCallBack() {
        @Override
        public void onResult(Object obj) {
            int index = (int) obj;
            Intent intent = new Intent(mContext, BigImage_CommActitivty.class);
            intent.putStringArrayListExtra("imgs", (ArrayList<String>) gameInfo.pictures);
            intent.putExtra("currentIndex", index);
            mContext.startActivity(intent);

        }
    };

    private void bindUI() {
        if (gameInfo == null) return;
        if (tv_text == null) return;
        tv_text.setText(gameInfo.desc);
        imgAdapter.setData(gameInfo.pictures);

    }

    public void setData(GameInfo gameInfo) {
        this.gameInfo = gameInfo;
        bindUI();
    }

}
