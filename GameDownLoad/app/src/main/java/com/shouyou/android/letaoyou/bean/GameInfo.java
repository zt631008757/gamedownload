package com.shouyou.android.letaoyou.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Administrator on 2018/12/19.
 */

public class GameInfo implements Serializable{

    public String id;//": 3,
    public String logo;//: "http://image.9game.cn/2018/7/12/20866065_.jpg",
    public String name;//: "女神联盟2（全球竞技）",
    public String subtitle;//: "多位唯美女神，由你来守护！",
    public String size;//: 297.7,
    public String version;//: "1.0.0",
    public List<String> pictures;
    public String manufacturer;//: null,
    public String desc;//: "简介:《女神联盟2》是一款拥有众多女神英雄、多重玩法创新的魔幻回合RPG手游，获Google Play海外91个国家首页力荐。由《GTA5》美术总监担纲监制营造精美女神魔幻世界。游戏在传统回合RPG的基础上，创新加入双怒气大招体验秒杀快感，微操策略让你体验不一样的回合RPG。",
    public String download_count;//: 0,
    public String download_link;//": "https://imgcdn.juefeng.com//sdks/870/870_dfzj_80142.apk",
    public String score;//": 10,
    public String is_new;//": 0,
    public String is_recommend;//": 0,
    public String sort;//": 0,
    public String type;//": 0,
    public String created_at;//": "2018-09-08 16:33:12",
    public String updated_at;//": "2018-09-24 17:36:01",
    public List<Tag> tags;
//             "pivot": {
//        "tag_id": 1,
//                "application_id": 3

    public class Tag implements Serializable
    {
        public String name;
    }

}
