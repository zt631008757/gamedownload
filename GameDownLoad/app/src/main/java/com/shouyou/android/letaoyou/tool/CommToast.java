package com.shouyou.android.letaoyou.tool;

import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.shouyou.android.letaoyou.R;

/**
 * Created by Administrator on 2016/8/21.
 */
public class CommToast {
    public static CommToast mToast = null;
    private static Toast toast = null;
    private static Handler mhandler = new Handler();
    private static Runnable r = new Runnable(){
        public void run() {
            toast.cancel();
            toast = null;
        };
    };

    public static void showToast(Context context, String tvString, int ... time){
        if(toast != null){
            Log.i("fulu","toast is not null!");
            return;
        }
        View layout = LayoutInflater.from(context.getApplicationContext()).inflate(R.layout.view_comm_toast,null);
        TextView text = (TextView) layout.findViewById(R.id.text);
        text.getBackground().setAlpha((int)(255*0.7));
        text.setText(tvString);
        mhandler.removeCallbacks(r);
        if(toast != null){
            toast.setText(tvString);
        }else{
            toast = new Toast(context.getApplicationContext());
            toast.setDuration(Toast.LENGTH_SHORT);
            toast.setView(layout);
            toast.show();
        }
        if(time!=null&&time.length>0)
        {
            toast.setDuration(time[0]);
            mhandler.postDelayed(r, time[0]);
        }
        else
        {
            mhandler.postDelayed(r, 1000);
        }
    }
}
