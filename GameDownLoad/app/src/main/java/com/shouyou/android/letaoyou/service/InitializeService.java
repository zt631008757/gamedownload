package com.shouyou.android.letaoyou.service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;

import com.shouyou.android.letaoyou.CrashHandler;

/**
 * Created by zt on 2018/8/13.
 * APP启动初始化操作，移动系统后台服务运行， 优化APP启动速度
 */
public class InitializeService extends IntentService {
    private static final String ACTION_INIT_WHEN_APP_CREATE = "com.anly.githubapp.service.action.INIT";

    public InitializeService() {
        super("InitializeService");
    }

    public static void start(Context context) {
        Intent intent = new Intent(context, InitializeService.class);
        intent.setAction(ACTION_INIT_WHEN_APP_CREATE);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_INIT_WHEN_APP_CREATE.equals(action)) {
                performInit();
            }
        }
    }

    //初始化操作
    private void performInit() {
        //程序异常捕获类
        CrashHandler crashHandler = CrashHandler.getInstance();
        crashHandler.init(getApplicationContext());
    }
}
