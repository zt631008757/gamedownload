package com.shouyou.android.letaoyou.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.shouyou.android.letaoyou.R;
import com.shouyou.android.letaoyou.interface_.CommCallBack;
import com.shouyou.android.letaoyou.tool.CommToast;
import com.shouyou.android.letaoyou.util.AnimUtil;
import com.shouyou.android.letaoyou.util.Util;

/**
 * Created by zt on 2018/6/30.
 * 通用填写弹框
 */

public class CommInput_Dialog extends Dialog implements View.OnClickListener {
    Context context;

    public CommInput_Dialog(@NonNull Context context) {
        super(context);
        this.context = context;
    }

    public CommInput_Dialog(@NonNull Context context, String title, String hintText, @StyleRes int themeResId) {
        super(context, themeResId);
        this.context = context;
        this.hintText = hintText;
        this.title = title;
    }

    protected CommInput_Dialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        this.context = context;
    }

    LinearLayout ll_content;
    View view_bg;
    EditText et_tag;
    TextView tv_cancel,content_text;
    TextView tv_submit;

    CommCallBack callBack;

    String title;
    String hintText;

    //设置输入回调
    public void setIntputCallBack(CommCallBack callBack) {
        this.callBack = callBack;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_addcollectinput);
        initView();
    }

    private void initView() {
        ll_content = (LinearLayout) findViewById(R.id.ll_content);
        view_bg = findViewById(R.id.view_bg);
        et_tag = findViewById(R.id.et_tag);
        tv_cancel = findViewById(R.id.tv_cancel);
        tv_submit = findViewById(R.id.tv_submit);
        content_text = findViewById(R.id.content_text);

        view_bg.setOnClickListener(this);
        ll_content.setOnClickListener(this);
        tv_cancel.setOnClickListener(this);
        tv_submit.setOnClickListener(this);

        AnimUtil.fadeIn(view_bg);
        AnimUtil.fadeIn(ll_content);

        setOnShowListener(new OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                Util.showKeyBoard(true, context, et_tag);
            }
        });

        content_text.setText(title);
        et_tag.setHint(hintText);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.view_bg:
                dismissWithAnim();
                break;
            case R.id.ll_content:
                //不处理，只消耗点击事件
                break;
            case R.id.tv_cancel:
                dismissWithAnim();
                break;
            case R.id.tv_submit:
                String text = et_tag.getText().toString().trim();
                if (TextUtils.isEmpty(text)) {
                    CommToast.showToast(context, "还没有输入哦~");
                    return;
                }
                callBack.onResult(text);
                break;
        }
    }

    public void dismissWithAnim() {
        AnimUtil.fadeOut(ll_content, null);
        AnimUtil.fadeOut(view_bg, new CommCallBack() {
            @Override
            public void onResult(Object obj) {
                dismiss();
            }
        });
    }

    @Override
    public void onBackPressed() {
        dismissWithAnim();
    }
}
