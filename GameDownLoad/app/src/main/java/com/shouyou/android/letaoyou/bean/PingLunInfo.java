package com.shouyou.android.letaoyou.bean;

/**
 * Created by Administrator on 2019/1/14.
 */

public class PingLunInfo {
    public String id; // ": 1,
    public String application_id; //": 5,
    public String user_id; //": 3,
    public String parent_id; //": 0,
    public String score; //": 2,
    public String content; //": "back",
    public String created_at; //": "2019-01-17 16:27:02",
    public String updated_at; //": "2019-01-17 16:27:02",
    public String likes_count; //": 0,
    public String has_like; //": 0,

    public UserInfo user;
}
