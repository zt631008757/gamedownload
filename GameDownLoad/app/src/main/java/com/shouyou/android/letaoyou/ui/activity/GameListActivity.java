package com.shouyou.android.letaoyou.ui.activity;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.shouyou.android.letaoyou.R;
import com.shouyou.android.letaoyou.adapter.FragmentViewPagerAdapter;
import com.shouyou.android.letaoyou.bean.TagInfo;
import com.shouyou.android.letaoyou.ui.fragment.GameListFragment;
import com.shouyou.android.letaoyou.ui.view.MultiStateView;
import com.shouyou.android.letaoyou.ui.view.MyScrollTabIndicator;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2018/6/20.
 */

public class GameListActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        index = getIntent().getIntExtra("index", 0);
        String listStr = getIntent().getStringExtra("tagList");
        tagInfoList = new Gson().fromJson(listStr, new TypeToken<List<TagInfo>>() {
        }.getType());
        setContentView(R.layout.activity_gamelist);
        initView();
        getData();
    }

    int index;
    List<TagInfo> tagInfoList;

    MultiStateView multiplestatusView;
    MyScrollTabIndicator mytabindicator;
    ViewPager viewpager;
    Handler handler = new Handler();

    private void initView() {
        setLeftImgClickListener();

        mytabindicator = findViewById(R.id.mytabindicator);
        viewpager = findViewById(R.id.viewpager);
    }

    private void getData() {

        //业务部分
        List<String> strs = new ArrayList<>();
        List<Fragment> fragmentList = new ArrayList<Fragment>();
        for (int i = 0; i < tagInfoList.size(); i++) {
            TagInfo tagInfo = tagInfoList.get(i);

            strs.add(tagInfo.name);
            GameListFragment fragment = new GameListFragment();
            fragment.tagId = tagInfo.id;
            fragmentList.add(fragment);

        }
        mytabindicator.setData(strs, false);

        FragmentViewPagerAdapter viewPagerAdapter = new FragmentViewPagerAdapter(getSupportFragmentManager(), fragmentList == null ? null : fragmentList);
        viewpager.setAdapter(viewPagerAdapter);
        viewpager.setOffscreenPageLimit(strs.size());
        viewpager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                mytabindicator.setSelect(position);
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
                mytabindicator.setOnscroll(arg0, arg1);
            }

            @Override
            public void onPageScrollStateChanged(int arg0) {
            }
        });
        //点击tabhost中的tab时，要切换下面的viewPager
        mytabindicator.setOnTabChangedListener(new MyScrollTabIndicator.OnTabChangedListener() {
            @Override
            public void onTabChanged(int index, boolean isClick) {
                viewpager.setCurrentItem(index);
            }
        });

        mytabindicator.setSelect(index);
        final int finalIndex = index;
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mytabindicator.setSelectedPosition(finalIndex);
            }
        }, 300);

    }
}
