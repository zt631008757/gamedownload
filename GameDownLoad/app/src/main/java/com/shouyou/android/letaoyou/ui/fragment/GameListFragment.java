package com.shouyou.android.letaoyou.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.jwenfeng.library.pulltorefresh.BaseRefreshListener;
import com.jwenfeng.library.pulltorefresh.PullToRefreshLayout;
import com.shouyou.android.letaoyou.R;
import com.shouyou.android.letaoyou.adapter.FaXian_ListAdapter;
import com.shouyou.android.letaoyou.adapter.GameListAdapter;
import com.shouyou.android.letaoyou.interface_.OkHttpCallBack;
import com.shouyou.android.letaoyou.manager.API_LoginManager;
import com.shouyou.android.letaoyou.responce.GetGameListResponce;
import com.shouyou.android.letaoyou.responce.GetWenZhangResponce;
import com.shouyou.android.letaoyou.ui.view.MultiStateView;

/**
 * Created by Administrator on 2018/8/20.
 */

public class GameListFragment extends BaseFragment {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mContext = getActivity();
        rootView = inflater.inflate(R.layout.fragment_gamelist, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
        getData();

    }

    public String tagId;

    Context mContext;
    View rootView;
    RecyclerView recyclerview;
    MultiStateView multiplestatusView;
    PullToRefreshLayout pulltorefreshlayout;
    GameListAdapter adapter;

    private void initView() {
        multiplestatusView = rootView.findViewById(R.id.multiplestatusView);
        multiplestatusView.setViewState(MultiStateView.ViewState.LOADING);
        multiplestatusView.setOnRetryListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                multiplestatusView.setViewState(MultiStateView.ViewState.LOADING);
                getData();
            }
        });


        recyclerview = rootView.findViewById(R.id.recyclerview);
        recyclerview.setLayoutManager(new LinearLayoutManager(mContext));
        adapter = new GameListAdapter(mContext, null);
        recyclerview.setAdapter(adapter);

        pulltorefreshlayout = rootView.findViewById(R.id.pulltorefreshlayout);
        pulltorefreshlayout.setCanRefresh(true);
        pulltorefreshlayout.setRefreshListener(new BaseRefreshListener() {
            @Override
            public void refresh() {
                getData();
            }

            @Override
            public void loadMore() {

            }
        });

    }

    private void getData()
    {
        API_LoginManager.games(mContext, tagId, "","0","","", new OkHttpCallBack() {
            @Override
            public void onSuccess(String result) {
                pulltorefreshlayout.finishRefresh();
                pulltorefreshlayout.finishLoadMore();
                multiplestatusView.setViewState(MultiStateView.ViewState.CONTENT);
                GetGameListResponce responce = new Gson().fromJson(result, GetGameListResponce.class);
                adapter.setData(responce.data);
                if(responce==null||responce.data==null||responce.data.size()==0)
                {
                    multiplestatusView.setViewState(MultiStateView.ViewState.EMPTY);
                }
            }

            @Override
            public void onFailure(String result) {
                pulltorefreshlayout.finishRefresh();
                pulltorefreshlayout.finishLoadMore();
                multiplestatusView.setViewState(MultiStateView.ViewState.EMPTY);
            }
        });
    }

}
