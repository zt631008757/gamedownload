package com.shouyou.android.letaoyou.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.shouyou.android.letaoyou.R;
import com.shouyou.android.letaoyou.bean.WenZhangInfo;
import com.shouyou.android.letaoyou.interface_.CommCallBack;
import com.shouyou.android.letaoyou.ui.activity.TuiJianDetailActivity;
import com.shouyou.android.letaoyou.util.GlideUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2018/8/22.
 */

public class TuiJian_GameListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context mContext;
    List<WenZhangInfo> list = new ArrayList<>();
    CommCallBack callBack;

    public TuiJian_GameListAdapter(Context mContext, CommCallBack callBack) {
        this.mContext = mContext;
        this.callBack = callBack;
    }

    public void setData(List<WenZhangInfo> list) {
        this.list.clear();
        this.list.addAll(list);
        notifyDataSetChanged();
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        ViewGroup viewGroup = (ViewGroup) mInflater.inflate(R.layout.item_tuijian_gamelist, null, false);
        ContentViewHolder holder = new ContentViewHolder(viewGroup);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ContentViewHolder viewHolder = (ContentViewHolder) holder;
        final WenZhangInfo info = list.get(position);
        if (position == 0) {
            viewHolder.view_space.setVisibility(View.VISIBLE);
        } else {
            viewHolder.view_space.setVisibility(View.GONE);
        }

        GlideUtil.displayImage(mContext, info.picture, viewHolder.iv_img, R.drawable.ico_default);
        if(info.game!=null) {
            GlideUtil.displayImage(mContext, info.game.application.logo, viewHolder.iv_head, R.drawable.ico_default);
        }
        viewHolder.tv_title.setText(info.title);
        viewHolder.tv_subtitle.setText(info.subtitle);
        if (TextUtils.isEmpty(info.subtitle)) {
            viewHolder.tv_subtitle.setVisibility(View.GONE);
        } else {
            viewHolder.tv_subtitle.setVisibility(View.VISIBLE);
        }

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, TuiJianDetailActivity.class);
                intent.putExtra("id", info.id);
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    class ContentViewHolder extends RecyclerView.ViewHolder {
        TextView tv_title, tv_subtitle;
        ImageView iv_img, iv_head;
        View view_space;

        public ContentViewHolder(View itemView) {
            super(itemView);
            tv_title = itemView.findViewById(R.id.tv_title);
            iv_img = itemView.findViewById(R.id.iv_img);
            iv_head = itemView.findViewById(R.id.iv_head);
            tv_subtitle = itemView.findViewById(R.id.tv_subtitle);
            view_space = itemView.findViewById(R.id.view_space);
        }
    }
}
