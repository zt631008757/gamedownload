package com.shouyou.android.letaoyou.bean;

import com.shouyou.android.letaoyou.responce.BaseResponce;

/**
 * Created by Administrator on 2018/12/6.
 */

public class UserInfo extends BaseResponce {
    public String id;
    public String admin_id;
    public String avatar;
    public String nickname;
    public String name;
    public String email;
    public String mobile;
    public String qq;
    public String wx;
    public String created_at;
    public String updated_at;

}
