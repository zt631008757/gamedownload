package com.shouyou.android.letaoyou.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.jwenfeng.library.pulltorefresh.BaseRefreshListener;
import com.jwenfeng.library.pulltorefresh.PullToRefreshLayout;
import com.shouyou.android.letaoyou.R;
import com.shouyou.android.letaoyou.adapter.TuiJian_GameListAdapter;
import com.shouyou.android.letaoyou.interface_.CommCallBack;
import com.shouyou.android.letaoyou.interface_.OkHttpCallBack;
import com.shouyou.android.letaoyou.manager.API_LoginManager;
import com.shouyou.android.letaoyou.responce.GetWenZhangResponce;
import com.shouyou.android.letaoyou.ui.view.MultiStateView;

/**
 * Created by Administrator on 2018/8/20.
 * 推荐
 */

public class Home_Fragment_TuiJian extends BaseFragment {

    private static Home_Fragment_TuiJian fragment = null;

    public static Home_Fragment_TuiJian newInstance() {
        if (fragment == null) {
            fragment = new Home_Fragment_TuiJian();
        }
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mContext = getActivity();
        rootView = inflater.inflate(R.layout.fragment_home_tuijian, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
        getData();
    }

    Context mContext;
    View rootView;
    RecyclerView recyclerview;
    MultiStateView multiplestatusView;
    SwipeRefreshLayout refreshlayout;

    TuiJian_GameListAdapter adapter;

    private void initView() {
        multiplestatusView = rootView.findViewById(R.id.multiplestatusView);
        multiplestatusView.setViewState(MultiStateView.ViewState.LOADING);
        multiplestatusView.setOnRetryListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                multiplestatusView.setViewState(MultiStateView.ViewState.LOADING);
                getData();
            }
        });


        recyclerview = rootView.findViewById(R.id.recyclerview);
        recyclerview.setLayoutManager(new LinearLayoutManager(mContext));
        adapter = new TuiJian_GameListAdapter(mContext, onItemClick);
        recyclerview.setAdapter(adapter);

        refreshlayout = rootView.findViewById(R.id.refreshlayout);
        refreshlayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getData();
            }
        });
    }


    private void getData() {
        API_LoginManager.articles(mContext, "", "1", new OkHttpCallBack() {
            @Override
            public void onSuccess(String result) {
                refreshlayout.setRefreshing(false);
                try {
                    multiplestatusView.setViewState(MultiStateView.ViewState.CONTENT);
                    GetWenZhangResponce responce = new Gson().fromJson(result, GetWenZhangResponce.class);
                    adapter.setData(responce.data);
                } catch (Exception e) {
                    multiplestatusView.setViewState(MultiStateView.ViewState.ERROR);
                }
            }

            @Override
            public void onFailure(String result) {
                refreshlayout.setRefreshing(false);
                multiplestatusView.setViewState(MultiStateView.ViewState.ERROR);
            }
        });
    }


    CommCallBack onItemClick = new CommCallBack() {
        @Override
        public void onResult(Object obj) {

        }
    };

}
