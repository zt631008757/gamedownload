package com.shouyou.android.letaoyou.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.view.View;
import android.widget.LinearLayout;

import com.shouyou.android.letaoyou.R;
import com.shouyou.android.letaoyou.interface_.CommCallBack;
import com.shouyou.android.letaoyou.util.AnimUtil;

/**
 * Created by Administrator on 2018/8/29.
 */

public class A_Model_Dialog extends Dialog implements View.OnClickListener {
    public A_Model_Dialog(@NonNull Context context) {
        super(context);
    }

    public A_Model_Dialog(@NonNull Context context, @StyleRes int themeResId) {
        super(context, themeResId);
    }

    protected A_Model_Dialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    LinearLayout ll_content;
    View view_bg;

    CommCallBack callBack;

    //设置输入回调
    public void setIntputCallBack(CommCallBack callBack) {
        this.callBack = callBack;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_commloading);
        initView();
    }

    private void initView() {
        ll_content = (LinearLayout) findViewById(R.id.ll_content);
        view_bg = findViewById(R.id.view_bg);

        view_bg.setOnClickListener(this);
        ll_content.setOnClickListener(this);

        AnimUtil.fadeIn(view_bg);
        AnimUtil.fadeIn(ll_content);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.view_bg:
                dismissWithAnim();
                break;
            case R.id.ll_content:
                //不处理，只消耗点击事件
                break;
        }
    }

    public void dismissWithAnim() {
        if (view_bg.getAnimation() != null) return;
        AnimUtil.fadeOut(ll_content, null);
        AnimUtil.fadeOut(view_bg, new CommCallBack() {
            @Override
            public void onResult(Object obj) {
                dismiss();
            }
        });
    }

    @Override
    public void onBackPressed() {
        dismissWithAnim();
    }
}
