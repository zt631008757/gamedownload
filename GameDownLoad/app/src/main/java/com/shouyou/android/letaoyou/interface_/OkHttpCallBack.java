package com.shouyou.android.letaoyou.interface_;

import com.shouyou.android.letaoyou.responce.BaseResponce;

/**
 * Created by zt on 2018/6/13.
 */

public interface OkHttpCallBack {
    void onSuccess(String result);
    void onFailure(String result);
}
