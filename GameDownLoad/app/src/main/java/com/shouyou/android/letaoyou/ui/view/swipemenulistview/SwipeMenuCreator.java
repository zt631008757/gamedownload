package com.shouyou.android.letaoyou.ui.view.swipemenulistview;


/**
 * 
 * @author baoyz
 * @date 2014-8-24
 *
 */
public interface SwipeMenuCreator {

	void create(SwipeMenu menu);
}
