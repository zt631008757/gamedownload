package com.shouyou.android.letaoyou.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.lzy.okgo.OkGo;
import com.lzy.okgo.db.DownloadManager;
import com.lzy.okgo.model.Progress;
import com.lzy.okgo.request.GetRequest;
import com.lzy.okserver.OkDownload;
import com.lzy.okserver.download.DownloadListener;
import com.lzy.okserver.download.DownloadTask;
import com.shouyou.android.letaoyou.R;
import com.shouyou.android.letaoyou.bean.GameInfo;
import com.shouyou.android.letaoyou.interface_.CommCallBack;
import com.shouyou.android.letaoyou.tool.CommToast;
import com.shouyou.android.letaoyou.tool.Log;
import com.shouyou.android.letaoyou.ui.activity.GameDetailActivity;
import com.shouyou.android.letaoyou.util.AppUtil;
import com.shouyou.android.letaoyou.util.GlideUtil;
import com.shouyou.android.letaoyou.util.Util;

import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static com.shouyou.android.letaoyou.MyApplication.context;

/**
 * Created by Administrator on 2018/8/22.
 */

public class GameListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context mContext;
    public List<GameInfo> list = new ArrayList<>();
    CommCallBack callBack;

    public GameListAdapter(Context mContext, CommCallBack callBack) {
        this.mContext = mContext;
        this.callBack = callBack;
        updateData();
    }

    public void updateData() {
        //这里是将数据库的数据恢复
        List<DownloadTask> values = OkDownload.restore(DownloadManager.getInstance().getAll());
        Log.i("values.size:" + values.size());
        notifyDataSetChanged();
    }


    public void setData(List<GameInfo> list) {
        this.list = list;
        notifyDataSetChanged();
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        ViewGroup viewGroup = (ViewGroup) mInflater.inflate(R.layout.item_gamelist, null, false);
        ContentViewHolder holder = new ContentViewHolder(viewGroup);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final ContentViewHolder viewHolder = (ContentViewHolder) holder;
        final GameInfo info = list.get(position);
        if (info == null) return;
        final String tagStr = info.id + "_" + info.name;

        //游戏部分
        GlideUtil.displayImage(mContext, info.logo, viewHolder.iv_gamelogo, R.drawable.ico_default);
        viewHolder.tv_game_name.setText(info.name);
        viewHolder.tv_size.setText(info.size + "M  |  ");
        viewHolder.tv_subname.setText(info.subtitle);


        if (OkDownload.getInstance().hasTask(tagStr)) {
            DownloadTask task = OkDownload.getInstance().getTask(tagStr);
            task.register(new ListDownloadListener(viewHolder, tagStr));
            viewHolder.setTask(task);
            viewHolder.setTag(tagStr);
            Progress progress = task.progress;
            viewHolder.refresh(progress);
        } else {
            viewHolder.setTask(null);
            viewHolder.setTag(null);
            viewHolder.rl_loading.setVisibility(View.GONE);  //下载中
            viewHolder.tv_download.setVisibility(View.VISIBLE);  //下载
            viewHolder.tv_finish.setVisibility(View.GONE);  //已完成
        }


        viewHolder.tv_download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DownloadTask task;
                if (!OkDownload.getInstance().hasTask(tagStr)) {
                    GetRequest<File> request = OkGo.get(info.download_link);
                    task = OkDownload.request(tagStr, request).register(new ListDownloadListener(viewHolder, tagStr));
                    task.extra1(info);
                    task.save();
                    viewHolder.setTask(task);
                    viewHolder.setTag(tagStr);
                    viewHolder.start();
                } else {
                    viewHolder.start();
                }
            }
        });

        viewHolder.rl_loading.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewHolder.task.pause();
            }
        });

        //安装/打开
        viewHolder.tv_finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Progress progress = viewHolder.task.progress;
                try {
                    AppUtil.install(mContext, progress.filePath);
                } catch (Exception e) {
                    CommToast.showToast(mContext, e.getMessage());
                }
            }
        });

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, GameDetailActivity.class);
                intent.putExtra("gameId", info.id);
                intent.putExtra("gameName", info.name);
                mContext.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }


    public class ContentViewHolder extends RecyclerView.ViewHolder {
        ImageView iv_gamelogo;
        TextView tv_game_name, tv_size, tv_tag, tv_subname, tv_download, tv_progress, tv_finish;
        ProgressBar progressbar;
        RelativeLayout rl_loading;


        public DownloadTask task;
        public String tag;

        public ContentViewHolder(View itemView) {
            super(itemView);
            iv_gamelogo = itemView.findViewById(R.id.iv_gamelogo);
            tv_game_name = itemView.findViewById(R.id.tv_game_name);
            tv_size = itemView.findViewById(R.id.tv_size);
            tv_tag = itemView.findViewById(R.id.tv_tag);
            tv_subname = itemView.findViewById(R.id.tv_subname);
            tv_download = itemView.findViewById(R.id.tv_download);
            progressbar = itemView.findViewById(R.id.progressbar);
            tv_progress = itemView.findViewById(R.id.tv_progress);
            rl_loading = itemView.findViewById(R.id.rl_loading);
            tv_finish = itemView.findViewById(R.id.tv_finish);

        }

        public void setTask(DownloadTask task) {
            this.task = task;
        }

        public void setTag(String tag) {
            this.tag = tag;
        }

        public String getTag() {
            return tag;
        }

        public void refresh(Progress progress) {
            rl_loading.setVisibility(View.GONE);  //下载中
            tv_download.setVisibility(View.GONE);  //下载
            tv_finish.setVisibility(View.GONE);  //已完成
            switch (progress.status) {
                case Progress.NONE:
                    tv_download.setText("下载");
                    tv_download.setVisibility(View.VISIBLE);  //下载
                    break;
                case Progress.PAUSE:
                    tv_download.setText("继续");
                    tv_download.setVisibility(View.VISIBLE);  //继续
                    break;
                case Progress.ERROR:
                    tv_download.setText("重试");
                    tv_download.setVisibility(View.VISIBLE);  //重试
                    break;
                case Progress.WAITING:
                    tv_progress.setText("等待中");
                    rl_loading.setVisibility(View.VISIBLE);  //等待中
                    break;
                case Progress.FINISH:
                    tv_finish.setText("安装");
                    tv_finish.setVisibility(View.VISIBLE);   //下载完成
                    break;
                case Progress.LOADING:    //下载中
                    progressbar.setProgress((int) (progress.fraction * 100));

                    Log.i("下载中" + progress.fraction * 100);
                    rl_loading.setVisibility(View.VISIBLE);  //下载中
                    tv_progress.setText(Util.numDecimalFormat(progress.fraction * 100 + "", 2) + "%");
                    break;
            }
        }

        public void start() {
            Log.i("start");
            Progress progress = task.progress;
            switch (progress.status) {
                case Progress.PAUSE:
                case Progress.NONE:
                case Progress.ERROR:
                    task.start();
                    break;
                case Progress.LOADING:
                    task.pause();
                    break;
                case Progress.FINISH:
//                    MusicPlayUtil.loadR(progress.filePath);
                    break;
            }
            refresh(progress);
        }
    }


    private class ListDownloadListener extends DownloadListener {

        public ContentViewHolder currentHolder;

        public ListDownloadListener(ContentViewHolder currentHolder, Object tag) {
            super(tag);
            this.currentHolder = currentHolder;

        }

        int reloadTimes = 0;  //重试次数 ， 下载失败 就重试  最多3次


        @Override
        public void onStart(Progress progress) {
            Log.i("开始下载");
            if (currentHolder == null) return;
            if (tag == currentHolder.getTag()) {
                currentHolder.refresh(progress);
            }
        }


        @Override
        public void onProgress(Progress progress) {
            if (currentHolder == null) return;
            if (tag == currentHolder.getTag()) {
                currentHolder.refresh(progress);
            }
        }

        @Override
        public void onError(Progress progress) {
            Throwable throwable = progress.exception;
            if (throwable != null) throwable.printStackTrace();
            Log.i("下载失败：" + progress.exception);
            CommToast.showToast(context, "下载失败，请重试");
//            if (reloadTimes < 1) {
//                Log.i("重试" + reloadTimes);
//                currentHolder.start();
//                reloadTimes++;
//            } else {
//                reloadTimes = 0;
//                CommToast.showToast(mContext, "下载失败，请重试");
//
////                    setHolderStatu(TYPE_DETAULT);
//            }
        }

        @Override
        public void onFinish(File file, Progress progress) {
            Log.i("下载完成");
//                EventBus.getDefault().post(new EventBus_DownLoadSuccess());
            if (currentHolder == null) return;
            if (tag == currentHolder.getTag()) {
//                    MediaPlayerUtil.play(progress.filePath, listener);
//                    ApiManager.updatePlayCount(context, currentHolder.voiceId, null);
            }
        }

        @Override
        public void onRemove(Progress progress) {
        }
    }
}
