package com.shouyou.android.letaoyou.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.shouyou.android.letaoyou.R;
import com.shouyou.android.letaoyou.bean.PingLunInfo;
import com.shouyou.android.letaoyou.interface_.CommCallBack;
import com.shouyou.android.letaoyou.interface_.OkHttpCallBack;
import com.shouyou.android.letaoyou.manager.API_LoginManager;
import com.shouyou.android.letaoyou.manager.UserManager;
import com.shouyou.android.letaoyou.tool.CommToast;
import com.shouyou.android.letaoyou.ui.view.MyRatingBar;
import com.shouyou.android.letaoyou.util.GlideUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2018/8/22.
 */

public class PingLunAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context mContext;
    List<PingLunInfo> list = new ArrayList<>();
    CommCallBack callBack;

    public PingLunAdapter(Context mContext, CommCallBack callBack) {
        this.mContext = mContext;
        this.callBack = callBack;
    }

    public void setData(List<PingLunInfo> list) {
        this.list = list;
        notifyDataSetChanged();
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater mInflater = LayoutInflater.from(mContext);
        ViewGroup viewGroup = (ViewGroup) mInflater.inflate(R.layout.item_pinglunlist, null, false);
        ContentViewHolder holder = new ContentViewHolder(viewGroup);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ContentViewHolder viewHolder = (ContentViewHolder) holder;
        final PingLunInfo info = list.get(position);
        viewHolder.tv_nickname.setText(info.user.nickname);
        GlideUtil.displayImage(mContext, info.user.avatar, viewHolder.iv_head, R.drawable.ico_home_head);
        viewHolder.tv_content.setText(info.content);
        viewHolder.tv_time.setText(info.created_at);
        viewHolder.ratingbar.setRating(Float.parseFloat(info.score));

        if ("1".equals(info.has_like)) {
            viewHolder.iv_zan.setImageResource(R.drawable.ico_yizan);
        } else {
            viewHolder.iv_zan.setImageResource(R.drawable.ico_zan);
        }

        viewHolder.tv_num.setText(info.likes_count);
        viewHolder.ll_zan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                zan(info.id);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    class ContentViewHolder extends RecyclerView.ViewHolder {
        TextView tv_content, tv_time, tv_nickname, tv_num;
        RatingBar ratingbar;
        ImageView iv_zan, iv_head;
        LinearLayout ll_zan;

        public ContentViewHolder(View itemView) {
            super(itemView);
            tv_content = itemView.findViewById(R.id.tv_content);
            tv_time = itemView.findViewById(R.id.tv_time);
            ratingbar = itemView.findViewById(R.id.ratingbar);
            tv_nickname = itemView.findViewById(R.id.tv_nickname);
            iv_zan = itemView.findViewById(R.id.iv_zan);
            tv_num = itemView.findViewById(R.id.tv_num);
            ll_zan = itemView.findViewById(R.id.ll_zan);
            iv_head = itemView.findViewById(R.id.iv_head);
        }
    }

    //赞
    private void zan(String id) {
        if (!UserManager.isLogin(mContext)) {
            UserManager.showLogin(mContext);
            CommToast.showToast(mContext, "请先登录");
            return;
        }

        API_LoginManager.like(mContext, id, new OkHttpCallBack() {
            @Override
            public void onSuccess(String result) {
                if (callBack != null) {
                    callBack.onResult(null);
                }
            }

            @Override
            public void onFailure(String result) {

            }
        });
    }
}
