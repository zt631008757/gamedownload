package com.shouyou.android.letaoyou.ui.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.google.gson.Gson;
import com.shouyou.android.letaoyou.R;
import com.shouyou.android.letaoyou.adapter.GameListAdapter;
import com.shouyou.android.letaoyou.adapter.H5GameAdapter;
import com.shouyou.android.letaoyou.interface_.OkHttpCallBack;
import com.shouyou.android.letaoyou.manager.API_LoginManager;
import com.shouyou.android.letaoyou.responce.GetGameListResponce;
import com.shouyou.android.letaoyou.ui.view.MultiStateView;

/**
 * Created by Administrator on 2018/6/20.
 */

public class FenLei_H5GameActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_h5youxi);
        initView();
        getData();
    }

    MultiStateView multiplestatusView;
    RecyclerView recyclerview;
    H5GameAdapter adapter;

    private void initView() {
        setTitle("H5小游戏");
        setLeftImgClickListener();

        multiplestatusView = (MultiStateView) findViewById(R.id.multiplestatusView);
        multiplestatusView.setViewState(MultiStateView.ViewState.LOADING);
        multiplestatusView.setOnRetryListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                multiplestatusView.setViewState(MultiStateView.ViewState.LOADING);
                getData();
            }
        });

        recyclerview = findViewById(R.id.recyclerview);
        recyclerview.setLayoutManager(new LinearLayoutManager(mContext));
        adapter = new H5GameAdapter(mContext, null);
        recyclerview.setAdapter(adapter);
        recyclerview.setHasFixedSize(true);
        recyclerview.setNestedScrollingEnabled(false);
    }

    private void getData() {
        API_LoginManager.games(mContext, "", "", "1", "", "", new OkHttpCallBack() {
            @Override
            public void onSuccess(String result) {
                multiplestatusView.setViewState(MultiStateView.ViewState.CONTENT);
                GetGameListResponce responce = new Gson().fromJson(result, GetGameListResponce.class);
                adapter.setData(responce.data);
                if (responce == null || responce.data == null || responce.data.size() == 0) {
                    multiplestatusView.setViewState(MultiStateView.ViewState.EMPTY);
                }
            }

            @Override
            public void onFailure(String result) {
                multiplestatusView.setViewState(MultiStateView.ViewState.EMPTY);
            }
        });
    }
}
