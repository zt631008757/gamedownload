package com.shouyou.android.letaoyou.ui.activity;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.shouyou.android.letaoyou.R;
import com.shouyou.android.letaoyou.adapter.GameListAdapter;
import com.shouyou.android.letaoyou.bean.GameInfo;
import com.shouyou.android.letaoyou.bean.WenZhangInfo;
import com.shouyou.android.letaoyou.interface_.OkHttpCallBack;
import com.shouyou.android.letaoyou.manager.API_LoginManager;
import com.shouyou.android.letaoyou.ui.view.MultiStateView;
import com.shouyou.android.letaoyou.util.GlideUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2018/6/20.
 */

public class TuiJianDetailActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wenzhangdetail);
        id = getIntent().getStringExtra("id");
        initView();
        initWebView();
        getData();
    }

    String id;
    WenZhangInfo wenZhangInfo;

    MultiStateView multiplestatusView;
    ImageView iv_img;
    TextView tv_tuijiantitle, tv_subtitle;
    RecyclerView recyclerview;
    GameListAdapter adapter;

    private void initView() {
        setTitle("游戏推荐");
        setLeftImgClickListener();

        multiplestatusView = (MultiStateView) findViewById(R.id.multiplestatusView);
        multiplestatusView.setViewState(MultiStateView.ViewState.LOADING);
        multiplestatusView.setOnRetryListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                multiplestatusView.setViewState(MultiStateView.ViewState.LOADING);
                getData();
            }
        });

        iv_img = findViewById(R.id.iv_img);
        tv_tuijiantitle = findViewById(R.id.tv_tuijiantitle);
        tv_subtitle = findViewById(R.id.tv_subtitle);

        recyclerview = findViewById(R.id.recyclerview);
        recyclerview.setLayoutManager(new LinearLayoutManager(mContext));
        adapter = new GameListAdapter(mContext, null);
        recyclerview.setAdapter(adapter);
    }

    private void getData() {
        API_LoginManager.article(mContext, id, new OkHttpCallBack() {
            @Override
            public void onSuccess(String result) {
                try {
                    multiplestatusView.setViewState(MultiStateView.ViewState.CONTENT);

                    wenZhangInfo = new Gson().fromJson(result, WenZhangInfo.class);

                    bindUI();

                    StringBuilder sb = new StringBuilder();
                    sb.append(getHtmlData(wenZhangInfo.body));
                    webView.loadDataWithBaseURL(null, sb.toString(), "text/html", "utf-8", null);
                } catch (Exception e) {

                }
            }

            @Override
            public void onFailure(String result) {
                multiplestatusView.setViewState(MultiStateView.ViewState.ERROR);
            }
        });
    }

    private void bindUI() {

        //推荐部分
        GlideUtil.displayImage(mContext, wenZhangInfo.picture, iv_img, -1);
        tv_tuijiantitle.setText(wenZhangInfo.title);
//        tv_content.setText(Html.fromHtml(wenZhangInfo.body));
        tv_subtitle.setText(wenZhangInfo.subtitle);
        if (TextUtils.isEmpty(wenZhangInfo.subtitle)) {
            tv_subtitle.setVisibility(View.GONE);
        } else {
            tv_subtitle.setVisibility(View.VISIBLE);
        }

        if (wenZhangInfo != null && wenZhangInfo.game != null && wenZhangInfo.game.application != null) {
            List<GameInfo> list = new ArrayList<>();
            list.add(wenZhangInfo.game.application);
            adapter.setData(list);
        }
    }

    WebView webView;

    private void initWebView() {
        webView = findViewById(R.id.webview);
        webView.setWebChromeClient(new MyWebClient());
        WebSettings webSettings = webView.getSettings();
        // 设置出现缩放工具
        webSettings.setBuiltInZoomControls(false);
        //自适应屏幕
        webSettings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        webSettings.setLoadWithOverviewMode(true);
        webView.setInitialScale(100);    //设置浏览器缩放百分比

        webSettings.setDatabaseEnabled(true);
        webSettings.setDomStorageEnabled(true);
        webSettings.setSaveFormData(false);
        webSettings.setAppCacheEnabled(false);
        webSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);
//         android 5.0以上默认不支持Mixed Content
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            webSettings.setMixedContentMode(WebSettings.MIXED_CONTENT_COMPATIBILITY_MODE);
        }

        //支持缩放，默认为true。
        webSettings.setSupportZoom(false);
        //调整图片至适合webview的大小
        webSettings.setUseWideViewPort(true);
        // 缩放至屏幕的大小
        webSettings.setLoadWithOverviewMode(true);
        //设置默认编码
        webSettings.setDefaultTextEncodingName("utf-8");
        //设置自动加载图片
        webSettings.setLoadsImagesAutomatically(true);
        //多窗口
        webSettings.supportMultipleWindows();
        //获取触摸焦点
        webView.requestFocusFromTouch();
        //允许访问文件
        webSettings.setAllowFileAccess(true);
        //开启javascript
        webSettings.setJavaScriptEnabled(true);
        //支持通过JS打开新窗口
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        //提高渲染的优先级
        webSettings.setRenderPriority(WebSettings.RenderPriority.HIGH);
        //支持内容重新布局
        webSettings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        //关闭webview中缓存
        webSettings.setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);

        String userAgent = webView.getSettings().getUserAgentString();
        if (!TextUtils.isEmpty(userAgent)) {
            webView.getSettings().setUserAgentString(userAgent
                    .replace("Android", "")
                    .replace("android", "")
                    + " cldc");
        }

        webView.setWebViewClient(new WebViewClient() {
            //当点击链接时,希望覆盖而不是打开新窗口
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);  //加载新的url
                return true;    //返回true,代表事件已处理,事件流到此终止
            }
        });
    }

    /**
     * 富文本适配
     */
    private String getHtmlData(String bodyHTML) {
        String head = "<head>"
                + "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=no\"> "
                + "<style>img{max-width: 100%; width:auto; height:auto;}</style>"
                + "</head>";
        return "<html>" + head + "<body>" + bodyHTML + "</body></html>";
    }


    class MyWebClient extends WebChromeClient {
        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            super.onProgressChanged(view, newProgress);
        }

        @Override
        public void onReceivedTitle(WebView view, String title) {
            super.onReceivedTitle(view, title);
        }
    }

    @Override
    protected void onDestroy() {
        if (webView != null) {
            webView.loadDataWithBaseURL(null, "", "text/html", "utf-8", null);
            webView.clearHistory();
            ((ViewGroup) webView.getParent()).removeView(webView);
            webView.destroy();
            webView = null;
        }
        super.onDestroy();
    }
}
