package com.shouyou.android.letaoyou.ui.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.shouyou.android.letaoyou.R;
import com.shouyou.android.letaoyou.interface_.OkHttpCallBack;
import com.shouyou.android.letaoyou.manager.API_LoginManager;
import com.shouyou.android.letaoyou.tool.CommToast;
import com.shouyou.android.letaoyou.ui.view.MultiStateView;

/**
 * Created by Administrator on 2018/6/20.
 */

public class FankuiActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fankui);
        initView();
    }

    MultiStateView multiplestatusView;
    EditText et_content, et_qq, et_phone;
    TextView tv_submit;

    private void initView() {
        setTitle("意见反馈");
        setLeftImgClickListener();
        et_content = findViewById(R.id.et_content);
        et_qq = findViewById(R.id.et_qq);
        et_phone = findViewById(R.id.et_phone);
        tv_submit = findViewById(R.id.tv_submit);

        tv_submit.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.tv_submit:
                feedback();
                break;
        }
    }

    public void feedback() {
        String content = et_content.getText().toString().trim();
        String qq = et_qq.getText().toString().trim();
        String phone = et_phone.getText().toString().trim();
        if (TextUtils.isEmpty(content)) {
            CommToast.showToast(mContext, "请输入您的建议");
            et_content.requestFocus();
            return;
        }

        API_LoginManager.feedback(mContext, content, qq, phone, new OkHttpCallBack() {
            @Override
            public void onSuccess(String result) {

            }

            @Override
            public void onFailure(String result) {

            }
        });
    }
}
